//
//  parsing.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/01/04.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//

import Foundation
import SwiftSoup
import Alamofire

class Parsing{
    /// 欲爬的版面網址
    private var urlSection: String = ""
    
    
    ///  爬手機版首頁
    func requestMenuHttp(urlString:String){
        guard let url = URL(string: urlString) else{
            return print("🐼 url is not valid!, url: \(urlString)")
        }
        
        let task = URLSession.shared
        task.configuration.timeoutIntervalForRequest = 3
        task.dataTask(with: url) { (data, response, error) in
            guard let data = data else{
                return
            }
            if error == nil{
                let urlContent = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                if let status = response as? HTTPURLResponse{
                    HTTPStatusCheck().statusCodeCheck(statusCode: status.statusCode)
                }
                self.parsingMenuHTML(html: urlContent! as String)
            }else{
                
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    let alert = AlertPop().alert(title: "網路請求失敗", message: error?.localizedDescription ?? "",
                                action: ["cancel":.cancel], handler: nil,
                                preferredStyle: .alert)
                    topController.present(alert,animated: true,completion: nil)
                }
            }
        }.resume()
    }
    
    ///  解析首頁之response的資訊
    func parsingMenuHTML(html: String){
        do{
            let doc: Document = try SwiftSoup.parse(html)
            let linkSet: [Element] = Array(try doc.select("a"))
            
            for item in linkSet{
                //  抓取網址
                var linkHref: String = try item.attr("href");
                //  抓取版名
                let linkText: String = try item.text();
                while linkHref.first == "/"{
                    linkHref.removeFirst()
                }
                if linkHref.contains("https://") == false{
                    linkHref = "https://" + linkHref
                }
                /**
                let linkOuterH: String = try! item.outerHtml();
                let linkInnerH: String = try! item.html();
                **/
                let newWebsite = SectionMenu(title: linkText,website: linkHref)
                arraySectionAll.append(newWebsite)
            }
            DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                let notification = NotificationCenter.default
                notification.post(name: Notification.Name("reloadMenu"), object: nil)
            })
        }catch let error as NSError{
            print("🐼\(error)")
        }
    }

    
    
    
    enum pageType: String {
        case Section = "short"
        case Thread  = "long"
    }
    /// 爬版面/貼文串內容
    func requestThreadHttp(html: String,typeParsing: pageType ){
        urlSection = html
        guard let url = URL(string: html) else{
            return print("🐼 url is not valid!, url: \(html)")
        }
        
        
        let task = URLSession.shared
        task.configuration.timeoutIntervalForRequest = 3
        task.dataTask(with: url) { (data, response, error) in
            
            if error == nil, let data = data{
                let urlContent = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                let statusCode = (response as! HTTPURLResponse).statusCode
                HTTPStatusCheck().statusCodeCheck(statusCode: statusCode)
                print(url)
                self.parsingThreadHTML(html: urlContent! as String, typeParsing: typeParsing)
            }else{
                guard let topController = UIApplication.shared.keyWindow?.rootViewController else{
                    return
                }
                let alert = AlertPop().alert(title: "網路請求失敗", message: error?.localizedDescription ?? "",
                                             action: ["cancel":.cancel],handler:{
                  let notification = NotificationCenter.default
                  notification.post(name: NSNotification.Name("recoverMenuIsUserInteractionEnabled"), object: nil)
                },preferredStyle: .alert)
                topController.present(alert,animated: true,completion: nil)
            }
        }.resume()
    }
    /// 解析版面/貼文串之response的資訊
    func parsingThreadHTML(html: String,typeParsing:pageType){
        do{
            let doc: Document = try SwiftSoup.parse(html)
            let threadBodySet: [Element] = Array(try doc.select("div"))
            for threadBody in threadBodySet{
                let bodyClass:String = try threadBody.attr("class")
     
                var threadLinkBuffer:[String] = []  //  原po文網址
                var contentBuffer:[String] = []     //  儲存該串所有貼文包含原po
                var detailBuffer:[postDetailNode] = []      //  儲存一則貼文的發文時間日期和ID
                var thumbnailBuffer:[String] = []   //  儲存縮圖url
                var sourceImgBuffer:[String] = []   //  儲存原圖url
                var serialNumBuffer:[String] = []   //  貼文編號
                
                
                switch bodyClass{
                //  一個新的獨立po文
                case "thread":
                    //  檢索內文
                    let commitBodySet:[Element] = Array(try threadBody.select("div"))
                    for commitBody in commitBodySet{
                        
                        let commitClass:String = try commitBody.attr("class")
                        switch commitClass{
                            //  原po
                        case "post threadpost":
                            
                            //  貼文編號
                            let serialNumber:String = try commitBody.attr("data-no")
                            serialNumBuffer.append("\(serialNumber)")
                            //  內文
                            let bodyOPSet = Array(try commitBody.select("div"))
                            for bodyOP in bodyOPSet{
                                let opClass:String = try bodyOP.attr("class")
                                switch opClass{
                                case "quote":
                                    let contentPo = try bodyOP.text().replacingOccurrences(of: " ", with: "\n")
                                    contentBuffer.append(contentPo)
                                default:    break
                                }
                            }
                            
                            //  檢索縮圖
                            let imgThumbnailBody = try commitBody.select("img")
                            let imgThumnail = try imgThumbnailBody.attr("src")
                            if imgThumnail != ""{   //  有附圖
                                thumbnailBuffer.append("https:\(imgThumnail)")
                            }else{          //  沒附圖
                                thumbnailBuffer.append("")
                            }
                            
                            //  檢索原圖
                            let imgSourceBody = try commitBody.select("a")
                            let imgSource = try imgSourceBody.attr("href")
                                //  檢查字尾
                            if  imgSource.hasSuffix(".png") &&
                                imgSource.hasSuffix(".jpg") &&
                                imgSource.hasSuffix(".gif"){
                                if imgSource != ""{   //  有附圖
                                    sourceImgBuffer.append("https:\(imgSource)")
                                    
                                }else{          //  沒附圖
                                    sourceImgBuffer.append("")
                                }
                            }
                            
                            //  原po文網址
                            let postLinkSet:[Element] = Array(try commitBody.select("a"))
                            for postLink in postLinkSet{
                                let postLinkClass:String = try postLink.attr("href")
                                if postLinkClass.hasPrefix("pixmicat.php?res"){
                                    var postlink = postLinkClass
                                    if typeParsing == .Section {
                                        
                                        postlink = SegueData.website + postlink
                                    }else if typeParsing == .Thread {
                                        postlink = urlSection + postlink
                                    }
                                        threadLinkBuffer.append(postlink)
                                }
                            }
                            
                            
                            
                            
                            
                            
                            
                            //  回文
                        case "post reply":
                            
                            //  貼文編號
                            let serialNumber:String = try commitBody.attr("data-no")
                            serialNumBuffer.append("\(serialNumber)")
                            //  內文
                            let bodyReplySet = Array(try commitBody.select("div"))
                            for bodyReply in bodyReplySet{
                                let replyClass:String = try bodyReply.attr("class")
                                switch replyClass{
                                case "quote":
                                    let contentPo = try bodyReply.text().replacingOccurrences(of: " ", with: "\n")
                                    contentBuffer.append(contentPo)
                                default:    break
                                }
                            }

                            //  檢索縮圖
                            let imgBody = try commitBody.select("img")
                            let img = try imgBody.attr("src")
                            if img != ""{   //  有附圖
                                thumbnailBuffer.append("https:\(img)")
                            }else{          //  沒附圖
                                thumbnailBuffer.append("")
                            }
                            //  檢索原圖
                            let imgSourceBody = try commitBody.select("a")
                            let imgSource = try imgSourceBody.attr("href")
                            
                            if imgSource != ""{   //  有附圖
                                //  檢查字尾
                                if  imgSource.hasSuffix(".png") ||
                                    imgSource.hasSuffix(".jpg") ||
                                    imgSource.hasSuffix(".gif"){
                                    sourceImgBuffer.append("https:\(imgSource)")
                                }
                            }else{          //  沒附圖
                                sourceImgBuffer.append("")
                            }
                        default:    break
                        }
                    }
                    
                    //  檢索貼文日期、時間、ID
                    let postInfoSet:[Element] = Array(try threadBody.select("span"))
                    for postInfo in postInfoSet{
                        let postClass:String = try postInfo.attr("class")
                        switch postClass{
                        case "now":
                            detailBuffer.append(analysisPostDetail(try postInfo.text()))
                        default:    break
                        }
                    }
                    
                    
                    /***********************************/
                    //  將資訊封裝放進array裡
                    var allPost:[postNode] = []
                    if contentBuffer.count > 0 {
                        for i in 0...contentBuffer.count-1{
                            //  建立貼文物件
                            let post:postNode = postNode(serialNo: serialNumBuffer[i],
                                                         contentPost: contentBuffer[i],
                                                         urlThumbnail: thumbnailBuffer[i],
                                                         detailPost: detailBuffer[i])
                            allPost.append(post)
                        }
                        
                        if typeParsing == .Section{
                            let link: String = previousSection
                            threadLinkBuffer.append(link)
                        }else if typeParsing == .Thread{
                            let link: String = urlSection 
                            threadLinkBuffer.append(link)
                        }
                        if UserDefaults.standard.string(forKey: "webMenu") == "https://komica2.net/m/"{
                            if threadLinkBuffer[0] != ""{
                                threadLinkBuffer[0] = threadLinkBuffer[0].replacingOccurrences(of: "index.htm", with: "")
                            }
                        }
                        //  建立貼文串物件
                        let newThread = threadNode(fromSection: SegueData.title,
                                                   linkOrigin: threadLinkBuffer.first ?? "",
                                                   post: allPost,
                                                   isExpaned: false)
                        if threadLinkBuffer.first != nil{
                            threadLinkBuffer.removeFirst()
                        }
                        
                        
                        
                        
                        
                        
                        switch typeParsing{
                        case .Section:
                            arrayThread.append(newThread)
                        case .Thread:
                            specificThread = newThread
                        }
                    }
                default:
                    break
                }
                
                
                
                
                
                
                
                
                
                
                
                threadLinkBuffer.removeAll()
                contentBuffer.removeAll()
                detailBuffer.removeAll()
                thumbnailBuffer.removeAll()
                sourceImgBuffer.removeAll()
                serialNumBuffer.removeAll()
                
            }
            
            
            
            
            
            
            
            
            
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
            
            
            
            DispatchQueue.main.async {
                self.callNotification(typeParsing)
            }
        }catch let error as NSError{
            print("🐼\(error)")
        }
    }
    
    
    func callNotification(_ type:pageType){
        let notification = NotificationCenter.default
        switch type{
        case .Section:
            if arrayThread.count > 0{
                for index in 0 ... arrayThread.count - 1{
                    arrayThread[index] = calculateRepeatID(arrayThread[index])
                }
                if newAllThreadsGroup != nil{
                    newAllThreadsGroup?.leave()
                }
            }else{
                let alert = AlertPop().alert(title: "解析失敗",
                                             message: "目前無法支援解析\(SegueData.title)版",
                                             action: ["確定": .default],handler: nil,
                                             preferredStyle: .alert)
                notification.post(name: Notification.Name("recoverMenuIsUserInteractionEnabled"),
                                  object: nil)
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.present(alert,animated: true,completion: nil)
                }
            }
        case .Thread:
            if specificThread != nil{
                /// 將該貼文紀錄進瀏覽紀錄中
                storeHistory().storingHistory(specificThread, type: .history)
                /// 計算該串各ID出現次數
                specificThread = calculateRepeatID(specificThread)
                /// 呼叫segue顯示指定貼文
                notification.post(name: Notification.Name("loadThreadLong"), object: nil)
            }else{
                print("🐼 specificThread is nil!")
                let alert = AlertPop().alert(title: "無法解析該串",
                                             message: "",
                                             action:["cancel":.default],handler: nil,
                                             preferredStyle: .alert)
                /// 恢復HistoryVC的tableView可以touch
                notification.post(name: NSNotification.Name("resetHistoryVCtableViewTouchable"), object: nil)
                //  取得目前最高顯示順位的ViewController
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.present(alert,animated: true,completion: nil)
                }
            }
        
        }
        
    }

    /// 計算該串各ID出現次數
    func calculateRepeatID(_ inputThread: threadNode) -> threadNode{
        var thread = inputThread
        ///  紀錄所有ID及它們出現的total次數
        var allAppearID:[String:Int] = [:]
        
        /// 從貼文串紀錄各id出現之次數
        for i in 0 ... thread.post.count - 1{
            let keyword = thread.post[i].detailPost.id.string
            allAppearID[keyword] = (allAppearID[keyword] == nil) ? 1 : allAppearID[keyword]! + 1
            /// 設置current
            thread.post[i].detailPost.id.current = i
        }
        /// 設定各帖文id的total
        for i in 0...thread.post.count - 1{
            let keyword = thread.post[i].detailPost.id.string
            thread.post[i].detailPost.id.total = allAppearID[keyword]!
        }
        
        
        ///  儲存貼文位置的array
        /// 將只出現一次的id排除在外
        var arrayIndex:[Int] = []
        for i in 0 ... thread.post.count - 1{
            if thread.post[i].detailPost.id.total > 1{
                arrayIndex.append(i)
            }else{
                arrayIndex.append(-1)
            }
        }
        
        
        
        
        /// 檢查是否擁有同id重複貼文的樓層
        if arrayIndex.count > 0{
            /// 反著從貼文底往回填充目前該貼文是第幾則
            arrayIndex.reverse()
            for i in 0...arrayIndex.count - 1 {
                if arrayIndex[i] > -1{
                    let index = arrayIndex[i]
                    let keyword = thread.post[index].detailPost.id.string
                    let current = allAppearID[keyword]!
                    thread.post[index].detailPost.id.current = current
                    if allAppearID[keyword]! > 1 {
                        allAppearID[keyword]! = current - 1
                    }else{
                        allAppearID[keyword]! = 1
                    }
                }
            }
            arrayIndex.reverse()
        }
        return thread
    }
    
    /// 將日期、時間、發文ID各別篩選出來
    func analysisPostDetail(_ string:String) -> postDetailNode{
        var Detail:[String?] = []
        var content = ""
        for char in string{
            content += "\(char)"

            if content.last == ")"{
                Detail.append(content)
                content = ""
            }else if content.last == " " && content.count > 1 {
                Detail.append(content)
                content = ""
            }
        }
        if content != ""{
            Detail.append(content)
        }
        Detail.append(content)
        /// 覆蓋掉uniqueID中的「ID:」
        
        Detail[2] = Detail[2]?.replacingOccurrences(of: "ID:", with: "")
        
        let newIDNode = idDetailNode(string: Detail[2] ?? "",
                                     previous: nil,
                                     current: nil,
                                     next: nil,
                                     total: 0)
        let newPostNode = postDetailNode(date: Detail[0] ?? "",time: Detail[1] ?? "",
                                         id: newIDNode, colorUniqueID:randomColor(seed: Detail[2]!))
        return newPostNode
    }
    
    ///  以貼文ID為種子產生隨機UIColor
    func randomColor(seed: String) -> UIColor {
        var total: Int = 0
        for u in seed.unicodeScalars {
            total += Int(UInt32(u))
        }
        srand48(total * 200)
        let r = CGFloat(drand48())
        
        srand48(total)
        let g = CGFloat(drand48())
        
        srand48(total / 200)
        let b = CGFloat(drand48())
        
        return UIColor(red: r, green: g, blue: b, alpha: 1)
    }

}
