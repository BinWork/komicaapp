//
//  appCheck.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/01/05.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//





//  檢查一些app的狀態問題
struct HTTPStatusCheck {
    func statusCodeCheck(statusCode: Int?){
        if let status = statusCode{
            var outputStr:String = ""
            let sufixStr:String  = "statusCode: \(status)"
            switch status{
            case 200...299: outputStr += "🐯 httpStatusCode OK!!" + sufixStr
            default:        outputStr += "🐼 httpStatusCode Not OK!!" + sufixStr
            }
            print(outputStr)
        }
    }
}
