//
//  globalVar.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/01/04.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//

import Foundation
import UIKit


var requestAPI = KomicaAPI()
/// from 首頁 to 版面
var newAllThreadsGroup: DispatchGroup?
/// from 版面 to 指定貼文串
var newPostsGroup: DispatchGroup?

/// 目前支援的網頁
var arrayAvailableWeb:[String] = [
    "sora.komica.org",
    "luna.komica.org",
    "aqua.komica.org"
]




///  Komica版面
var arraySectionAll:[SectionMenu] = []
/// 「貼文串」列表
var arrayThread:[threadNode] = []
/// 指定貼文(例以No.XXXXX為原po的完整貼文串)
var specificThread:threadNode!
/// 書籤的貼文紀錄
var arrayBookmark:[postSummaryNode] = []
/// 瀏覽過的貼文的歷史紀錄
/// 貼文的瀏覽紀錄
var arrayHistory:[postSummaryNode] = []


/// 傳送資料用的
var SegueData:SectionMenu = SectionMenu(title:"",website:"")

/// 上一個所處版面
var previousSection: String = ""
/// 上一個所處貼文
var previousThread: String = ""







/// 首頁版面
struct SectionMenu{
    var title:String
    var website:String
    init(title: String, website: String) {
        self.title = title
        self.website = website
    }
}
/// 貼文串大綱(瀏覽紀錄、貼文書籤)
struct postSummaryNode: Codable {
    /// 版名
    var sectionName:String
    /// 貼文網址
    var urlThread:String
    /// 貼文原po圖
    var urlThumbnail:String?
    /// 貼文原po
    var contentThread:String
    /// 最後瀏覽時間
    var dateVisited:String
    init(sectionName:String, urlThread:String,
         urlThumbnail:String?, contentThread:String, dateVisited:String) {
        self.sectionName = sectionName
        self.urlThread = urlThread
        self.urlThumbnail = urlThumbnail
        self.contentThread = contentThread
        self.dateVisited = dateVisited
    }
}










struct threadNode{
    /// 來源版面
    var fromSection:String
    /// 貼文網址
    var linkOrigin:String?
    /// 貼文串
    var post:[postNode]
    /// 是否展開
    var isExpaned:Bool
}
struct postNode{
    /// 貼文流水號
    var serialNo:String
    /// 貼文內容
    var contentPost:String
    /// 附圖縮網址
    var urlThumbnail:String
    /// 日期、時間、發文ID
    var detailPost:postDetailNode
}
struct postDetailNode{
    /// 發文日期
    var date:String
    /// 發文時間
    var time:String
    /// 發文者unique ID
    var id: idDetailNode
    /// 屬於發文者unique ID的unique Color
    var colorUniqueID:UIColor?
}
struct idDetailNode{
    /// 貼文ID
    var string: String
    /// 同ID下、貼文串內之上一篇的位置
    var previous: Int?
    /// 同ID下、貼文串之出現第幾次
    var current: Int?
    /// 同ID下、貼文串內之下一篇的位置
    var next: Int?
    /// 該ID出現總次數
    var total: Int!
    
}
