//
//  extension.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/01/04.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//

import UIKit

//
//  可攜帶IndexPath的PanGestureRecognizer
//
class CustomPanGestureRecognizer: UIPanGestureRecognizer{
    var indexPathImage:IndexPath?
    var rectImage:CGRect?
}
//
//  震動效果
//
extension UIView {
    // Using CAMediaTimingFunction
    func shake(duration: TimeInterval = 0.5, values: [CGFloat]) {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = duration // You can set fix duration
        animation.values = values  // You can set fix values here also
        self.layer.add(animation, forKey: "shake")
    }
    // Using SpringWithDamping
    func shake(duration: TimeInterval = 0.5, xValue: CGFloat = 12, yValue: CGFloat = 0) {
        self.transform = CGAffineTransform(translationX: xValue, y: yValue)
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform.identity
        }, completion: nil)
        
    }
    // Using CABasicAnimation
    func shake(duration: TimeInterval = 0.05, shakeCount: Float = 6, xValue: CGFloat = 12, yValue: CGFloat = 0){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = duration
        animation.repeatCount = shakeCount
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - xValue, y: self.center.y - yValue))
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + xValue, y: self.center.y - yValue))
        self.layer.add(animation, forKey: "shake")
    }
    
}
extension UIViewController{
    
    /// 震動功能 (強度: .light 小, .medium 中, .heavy 強)
    func Vibration(_ style:UIImpactFeedbackGenerator.FeedbackStyle){
        let vibration = UIImpactFeedbackGenerator(style: .light)
        vibration.impactOccurred()
    }
    
    
    /**
     頂端popup提示
     
     - Parameter content: 欲呈現的內文
     - Parameter Delay: popUp停留的時間 (預設為 1.0)
     - Parameter popUpDuration: popUp的動畫時間 (預設為 0.5)
     - Parameter popOffDuration: popUp的動畫時間 (預設為 0.5)
     
     */
    func alertPopUp(content:String, Delay: TimeInterval = 1.0, color: UIColor = UIColor.lightGray,
                    popUpDuration: TimeInterval = 0.5, popOffDuration: TimeInterval = 0.5){
        let extraHeight:CGFloat = {
            let factor:CGFloat = 15
            return CGFloat(content.filter { $0 == "\n" }.count) * factor
        }()
        let extraWidth:CGFloat = {
            let factor:CGFloat = 10
            return CGFloat(content.count / 2) * factor
        }()
        let sizeHeight:CGFloat = 35 + extraHeight
        let sizeWidth:CGFloat = 80 + extraWidth
        
        let lblSaved:UILabel = {
            let lbl = UILabel(frame: CGRect(x: UIScreen.main.bounds.midX - sizeWidth/2,
                                            y: -sizeHeight,
                                            width: sizeWidth,
                                            height: sizeHeight))
            lbl.text = content
            lbl.textAlignment = .center
            lbl.textColor = .black
            lbl.numberOfLines = 0
            lbl.backgroundColor = color.withAlphaComponent(1)
            lbl.layer.cornerRadius = lbl.frame.size.height/2
            lbl.layer.masksToBounds = true
            return lbl
        }()
        //  取得目前最高顯示順位的ViewController
        if let topController = UIApplication.shared.keyWindow?.rootViewController {
            topController.view.addSubview(lblSaved)
        }
        let barHeight = self.navigationController?.navigationBar.frame.height
        let transformLengthY:CGFloat = sizeHeight + barHeight! + 50
        
        let transformShow = CGAffineTransform(translationX: self.view.frame.minX,
                                              y: transformLengthY)
        let transformHide = CGAffineTransform(translationX: self.view.frame.minX,
                                              y: -transformLengthY)
        
        
        UIView.animate(withDuration: popUpDuration, delay: 0.0, usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.8, options: .curveEaseInOut,
                       animations: {
                        lblSaved.transform = transformShow
        }, completion: { finish in
            UIView.animate(withDuration: popOffDuration, delay: Delay, usingSpringWithDamping: 0.5,
                           initialSpringVelocity: 0.8, options: .curveEaseInOut,
                           animations: {
                            lblSaved.transform = transformHide
            }, completion: { finish in
                lblSaved.removeFromSuperview()
            })
        })
    }
}
extension UIColor {
    var themeDark: UIColor{
        return UIColor(displayP3Red: 30/255, green: 30/255, blue: 30/255, alpha: 1.0)
    }
    var inverted: UIColor {
        var r: CGFloat = 0.0, g: CGFloat = 0.0, b: CGFloat = 0.0, a: CGFloat = 0.0
        UIColor.red.getRed(&r, green: &g, blue: &b, alpha: &a)
        return UIColor(red: (1 - r), green: (1 - g), blue: (1 - b), alpha: a) // Assuming you want the same alpha value.
    }
}
extension UITableView{
    func AnimationReloadData(){
        let AnimationDuration:TimeInterval = 1.0
        self.reloadData()
        
        let cells = self.visibleCells
        let tableHeight: CGFloat = self.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            a.isUserInteractionEnabled = false
            self.isHidden = false
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: AnimationDuration, delay: 0.04 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .transitionCrossDissolve, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: { finish in
                a.isUserInteractionEnabled = true
            })
            
            index += 1
        }
    }
}

