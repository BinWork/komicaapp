//
//  storeHistory.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/02/16.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//

import Foundation
enum SummaryType {
    case history
    case bookmark
}
/// 瀏覽紀錄&書籤處理用
class storeHistory{
    private var storingCategory:SummaryType?
    /// 將該貼文紀錄進瀏覽紀錄中
    func storingHistory(_ thread: threadNode,type storingType: SummaryType){
        let sectionName = thread.fromSection
        let url = thread.linkOrigin ?? ""
        let urlThumbnail = thread.post.first?.urlThumbnail
        let content:String = {
            var str:String = ""
            let arrayContent = Array((thread.post.first?.contentPost)!)
            if arrayContent.count < 50{
                str = (thread.post.first?.contentPost)!
                return str
            }else{
                for i in 0...49{
                    str += String(arrayContent[i])
                }
            }
            return str
        }()
        let date = getDate()
        
        let newThread = postSummaryNode.init(sectionName:   sectionName,
                                             urlThread:     url,
                                             urlThumbnail:  urlThumbnail,
                                             contentThread: content,
                                             dateVisited:   date)
        
        isRepeatStoring(newThread, type: storingType)

        
    }
    
    func storingBookmark(_ summary: postSummaryNode, type storingType: SummaryType){
        isRepeatStoring(summary, type: storingType)
        
    }
    /// 檢查是否重複儲存瀏覽紀錄
    func isRepeatStoring(_ newThread: postSummaryNode, type storingType: SummaryType) {
        switch storingType {
        case .history:
            if arrayHistory.count > 0 {
                for index in 0...arrayHistory.count - 1{
                    /// 重複瀏覽的場合將該紀錄刷新到index:0的位置
                    if newThread.urlThread == arrayHistory[index].urlThread{
                        var newestThread = arrayHistory.remove(at: index)
                        newestThread.dateVisited = newThread.dateVisited
                        arrayHistory.insert(newestThread, at: 0)
                        return
                    }
                }
            }
            if newThread.urlThread != ""{
                arrayHistory.insert(newThread, at: 0)
                /// 若為新的瀏覽紀錄則對「threadVisited」的紀錄做+1
                addNewVisited()
            }
            
            
            
            
        case .bookmark:
            if arrayBookmark.count > 0 {
                for index in 0...arrayBookmark.count - 1{
                    /// 重複瀏覽的場合將該紀錄刷新到index:0的位置
                    if newThread.urlThread == arrayBookmark[index].urlThread{
                        var newestThread = arrayBookmark.remove(at: index)
                        newestThread.dateVisited = newThread.dateVisited
                        arrayBookmark.insert(newestThread, at: 0)
                        return
                    }
                }
            }
            if newThread.urlThread != ""{
                arrayBookmark.insert(newThread, at: 0)
                /// 若為新的瀏覽紀錄則對「threadVisited」的紀錄做+1
                addNewVisited()
            }
        }
        
    }
    /// 儲存瀏覽紀錄在手機裡
//    func savingThread(type storingType: SummaryType){
//        var dataArray:Data?
//        switch storingType {
//        case .history:
//            UserDefaults.standard.removeObject(forKey: "arrayHistory")
//            dataArray = try? PropertyListEncoder().encode(arrayHistory)
//            UserDefaults.standard.set(dataArray, forKey: "arrayHistory")
//        case .bookmark:
//            UserDefaults.standard.removeObject(forKey: "arrayBookmark")
//            dataArray = try? PropertyListEncoder().encode(arrayBookmark)
//            UserDefaults.standard.set(dataArray, forKey: "arrayBookmark")
//        }
//        dataArray = nil
//    }
    /// 清除所有瀏覽紀錄
    func clearHistoryStore(){
        arrayHistory.removeAll()
        UserDefaults.standard.removeObject(forKey: "arrayHistory")
    }
    /// 清除所有書籤
    func clearBookmarkStore(){
        arrayBookmark.removeAll()
        UserDefaults.standard.removeObject(forKey: "arrayBookmark")
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /// 紀錄總共瀏覽多少筆貼文紀錄
    func addNewVisited(){
        guard UserDefaults.standard.object(forKey: "threadVisited") != nil else{
            UserDefaults.standard.set(1, forKey: "threadVisited")
            return
        }
        let threadVisited = UserDefaults.standard.integer(forKey: "threadVisited")
        UserDefaults.standard.set((threadVisited + 1), forKey: "threadVisited")
    }
    /// 轉換時間格式
    func getDate() -> String{
        let currentDate = Date()
        let formatFirst = DateFormatter()
        let formatLast = DateFormatter()
        formatFirst.dateFormat = "E dd.MM.yy"
        formatLast.dateFormat = "HH:mm"
        let strDate =   formatFirst.string(from: currentDate) + " " +
                        formatLast.string(from: currentDate)
        return strDate
    }


}
