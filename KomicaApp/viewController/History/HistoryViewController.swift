//
//  HistoryViewController.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/02/16.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//

import UIKit
import UserNotifications
import SDWebImage
import SafariServices


class HistoryCell: UITableViewCell {
    /// 版面名稱
    @IBOutlet weak var lblSectionName: UILabel!
    /// 縮圖
    @IBOutlet weak var imgThumbnail: UIImageView!
    /// 內文
    @IBOutlet weak var lblContent: UILabel!
    /// 瀏覽日期
    @IBOutlet weak var lblDateVisited: UILabel!
    /// 縮圖width
    @IBOutlet weak var widthThumbnail: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        //  accessoryType = .disclosureIndicator
        //  accessoryView?.backgroundColor = .clear
        imgThumbnail.contentMode = .scaleAspectFill
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected{
            
        }else{
            lblSectionName.textColor = .white
            lblContent.textColor = .lightGray
            lblDateVisited.textColor = .lightGray
            backgroundColor = .clear
        }

        // Configure the view for the selected state
    }
}
class HistoryViewController: UIViewController {
    
    

    /// 目前呈現的是歷史紀錄還是書籤
    private var currentPresent:SummaryType = .history
    
    
    @IBOutlet weak var historyTableView: UITableView!
    @IBOutlet weak var segmentList: UISegmentedControl!
    /// 瀏覽紀錄 & 書籤
    @IBAction func actionSegment(_ sender: UISegmentedControl) {
        setNavigationTitle()
        Vibration(.light)
    }
    /// 右上角的動作按鈕
    @IBOutlet weak var btnTopRight: UIButton!
    @IBAction func actionTopRight(_ sender: UIButton) {
        switch currentPresent {
        case .history:
            clearHistory()
        case .bookmark:
            clearBookmark()
            break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        historyTableView.tableFooterView = UIView()
        
        let icon = UIImageView.init(image: UIImage(named: "icon_App"))
        icon.alpha = 0.1
        historyTableView.backgroundView?.contentMode = .scaleAspectFit
        historyTableView.backgroundView = icon
        
        notificationSet()
        segmentList.selectedSegmentIndex = 0
        currentPresent = .history
        setNavigationTitle()
    }
    override func viewWillAppear(_ animated: Bool) {
        setNavigationTitle()
    }
    override func viewWillDisappear(_ animated: Bool) {
        if isMovingFromParent{
            Vibration(.light)
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        historyTableView.flashScrollIndicators()
    }
    override func viewDidDisappear(_ animated: Bool) {
        historyTableView.isUserInteractionEnabled = true
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
    }
    /// 根據segment顯示navigationTitle
    private func setNavigationTitle(){
        var title = "瀏覽紀錄(\(arrayHistory.count))"
        let animateDelay: TimeInterval = 0.3
        switch segmentList.selectedSegmentIndex{
        case 0:
            if currentPresent != .history{
                currentPresent = .history
                title = "瀏覽紀錄(\(arrayHistory.count))"
                UIView.animate(withDuration: animateDelay, animations: {
                    [weak self] in
                        self?.btnTopRight.alpha = 0.0
                    }, completion: { (finish:Bool) in
                        UIView.animate(withDuration: animateDelay, animations: {
                            [weak self] in
                            self?.btnTopRight.setTitle("清除所有瀏覽紀錄", for: .normal)
                            self?.btnTopRight.alpha = 1.0
                        })
                })
            }
        case 1:
            if currentPresent != .bookmark{
                currentPresent = .bookmark
                title = "書籤(\(arrayBookmark.count))"
                UIView.animate(withDuration: animateDelay, animations: {
                    [weak self] in
                    self?.btnTopRight.alpha = 0.0
                    }, completion: {(finish:Bool) in
                        UIView.animate(withDuration: animateDelay, animations: {
                            [weak self] in
                            self?.btnTopRight.setTitle("清除所有書籤", for: .normal)
                            self?.btnTopRight.alpha = 1.0
                        })
                })
            }
        default:    break
        }
        historyTableView.reloadData()
        UIView.animate(withDuration: 0.15, animations: {
            [weak self] in
            self?.navigationItem.titleView?.alpha = 0.0
        }, completion: { [weak self](finish:Bool) in
            self?.navigationItem.title = title
            UIView.animate(withDuration: 0.15, animations: {
                [weak self] in
                self?.navigationItem.titleView?.alpha = 1.0
            })
        })
        
    }
    
    
    func notificationSet(){
        let notification = NotificationCenter.default
        notification.addObserver(self, selector: #selector(loadThreadLong),
                                 name: NSNotification.Name(rawValue: "loadThreadLong"), object: nil)
        
        notification.addObserver(self, selector: #selector(resetHistoryVCtableViewTouchable),
                                 name: NSNotification.Name(rawValue: "resetHistoryVCtableViewTouchable"), object: nil)
    }
    @objc func loadThreadLong(){
        let storyBoard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "ThreadView") as! ThreadViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    @objc func resetHistoryVCtableViewTouchable(){
        historyTableView.isUserInteractionEnabled = true
    }
    
    
    func clearHistory(){
        let alertView = UIAlertController.init(title: "清除全部紀錄?", message: nil, preferredStyle: .alert)
        
        let actionYes = UIAlertAction.init(title: "確定", style: .default, handler: { [weak self] action in
            let classHistory = storeHistory()
            classHistory.clearHistoryStore()
            self?.alertPopUp(content: "已清除")
            self?.historyTableView.reloadData()
            self?.setNavigationTitle()
        })
        actionYes.setValue(UIColor.red, forKey: "titleTextColor")
        let actionCancel = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
        alertView.addAction(actionYes)
        alertView.addAction(actionCancel)
        if arrayHistory.count > 0{
            present(alertView,animated: true, completion: nil)
        }else{
            alertPopUp(content: "無任何紀錄")
        }
    }
    func clearBookmark(){
        let alertView = UIAlertController.init(title: "清除全部書籤?", message: nil, preferredStyle: .alert)
        
        let actionYes = UIAlertAction.init(title: "確定", style: .default, handler: { [weak self] action in
            let classHistory = storeHistory()
            classHistory.clearBookmarkStore()
            self?.alertPopUp(content: "已清除")
            self?.historyTableView.reloadData()
            self?.setNavigationTitle()
        })
        actionYes.setValue(UIColor.red, forKey: "titleTextColor")
        let actionCancel = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
        alertView.addAction(actionYes)
        alertView.addAction(actionCancel)
        if arrayBookmark.count > 0{
            present(alertView,animated: true, completion: nil)
        }else{
            alertPopUp(content: "無任何紀錄")
        }
    }
}

extension HistoryViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch currentPresent {
        case .history:
            return arrayHistory.count
        case .bookmark:
            return arrayBookmark.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HistoryCell
        
        let index = indexPath.row
        var thread:postSummaryNode?
        switch currentPresent {
        case .history:
            thread = arrayHistory[index]
        case .bookmark:
            thread = arrayBookmark[index]
        }
        guard thread != nil else{
            return cell
        }
        
        
        cell.lblSectionName.text = thread?.sectionName
        cell.lblDateVisited.text = thread?.dateVisited
        cell.lblContent.text = thread?.contentThread
        if let urlImg = URL(string: thread?.urlThumbnail! ?? ""){
            cell.widthThumbnail.constant = 73.5
            cell.imgThumbnail.sd_setImage(with: urlImg,
                                          placeholderImage: nil)

            cell.imgThumbnail.layer.cornerRadius = cell.imgThumbnail.frame.size.height/2
            cell.imgThumbnail.layer.masksToBounds = true
        }else{
            cell.widthThumbnail.constant = 0.0
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Vibration(.light)
        
        var urlThread: String = ""
        switch currentPresent {
        case .history:
            urlThread = arrayHistory[indexPath.row].urlThread
        case .bookmark:
            urlThread = arrayBookmark[indexPath.row].urlThread
        }
//        Parsing().requestThreadHttp(html: urlThread, typeParsing: .Thread)
        guard let url = URL(string: urlThread) else { return }
        let caution = UIAlertController(title: "即將跳轉網頁版?", message: urlThread,
                                        preferredStyle: .alert)
        let ok = UIAlertAction(title: "前往", style: .default, handler: { action in
            self.openInSafari(linkOfWebsite: url)
        })
        let cancel = UIAlertAction(title: "取消", style: .default, handler: nil)
        caution.addAction(cancel)
        caution.addAction(ok)
        present(caution, animated: true, completion: nil)
    }
    
    /// 用內建瀏覽器safari打開url
    private func openInSafari(linkOfWebsite url: URL){
        let vcSafari = SFSafariViewController(url: url)
        present(vcSafari, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        ///  刪除該筆紀錄
        let deleteFromHistory: UIContextualAction = {
            let action = UIContextualAction(style: .normal, title: nil, handler: {(action,view,handler) in
                let alertView = UIAlertController.init(title: "清除該紀錄?", message: nil, preferredStyle: .alert)
                
                let actionYes = UIAlertAction.init(title: "確定", style: .default, handler: { action in
                    let index = indexPath.row
                    switch self.currentPresent{
                    case .history:
                            arrayHistory.remove(at: index)
                    case .bookmark:
                            arrayBookmark.remove(at: index)
                    }
                    self.historyTableView.deleteRows(at: [indexPath], with: .automatic)
                    self.setNavigationTitle()
                    self.alertPopUp(content: "已清除")
                    
                })
                actionYes.setValue(UIColor.red, forKey: "titleTextColor")
                let actionCancel = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
                actionCancel.setValue(UIColor.black, forKey: "titleTextColor")
                alertView.addAction(actionYes)
                alertView.addAction(actionCancel)
                handler(true)
                self.present(alertView,animated: true, completion: nil)
            })
            return action
            }()
        deleteFromHistory.image = UIImage(named: "icon_Delete")
        deleteFromHistory.backgroundColor = UIColor(displayP3Red: 180/255, green: 0, blue: 0, alpha: 1)
        return UISwipeActionsConfiguration(actions: [deleteFromHistory])
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard currentPresent == .history else{
            return UISwipeActionsConfiguration(actions: [])
        }
        
        ///  加入書籤
        let addToBookmark: UIContextualAction = {
            [weak self] in
            let action = UIContextualAction(style: .normal, title: nil, handler: {[weak self](action,View,handler) in
                let alertView = UIAlertController.init(title: "加入書籤?", message: nil, preferredStyle: .alert)
                
                let actionYes = UIAlertAction.init(title: "確定", style: .default, handler: { [weak self] action in
                    let index = indexPath.row
                    arrayBookmark.append(arrayHistory[index])
                    var classHistory:storeHistory? = storeHistory()
                    classHistory?.storingBookmark(arrayHistory[index],type: .bookmark)
                    self?.setNavigationTitle()
                    self?.alertPopUp(content: "加入書籤")
                    classHistory = nil
                })
                actionYes.setValue(self?.view.tintColor, forKey: "titleTextColor")
                let actionCancel = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
                actionCancel.setValue(UIColor.black, forKey: "titleTextColor")
                alertView.addAction(actionYes)
                alertView.addAction(actionCancel)
                handler(true)
                self?.present(alertView,animated: true, completion: nil)
            })
            return action
            }()
        addToBookmark.image = UIImage(named: "icon_Bookmark")
        addToBookmark.backgroundColor = view.tintColor
        return UISwipeActionsConfiguration(actions: [addToBookmark])
    }

}
