//
//  TrackingViewController.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/02/19.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//

import UIKit
class TrackTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        backgroundColor = .clear
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        if selected == false{
            backgroundColor = .clear
        }
        
        tintColor = .clear
    }
}
class TrackingViewController: UIViewController {
    
    /// 欲顯示追蹤的項目
    private var list:[String:Any?] = ["App啟動次數": 0, "圖片下載次數": 0]
    override func viewWillAppear(_ animated: Bool) {
        loadStatus()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    /// 載入tracking的資料
    func loadStatus(){
        if let launchCount = UserDefaults.standard.object(forKey: "appLaunchCount") as? Int{
            list["App啟動次數"] = launchCount
        }
        
        if let imgSavedCount = UserDefaults.standard.object(forKey: "imgSavedCount") as? Int{
            list["圖片下載次數"] = imgSavedCount
        }
        if let threadVisited = UserDefaults.standard.object(forKey: "threadVisited") as? Int{
            list["貼文瀏覽總數"] = threadVisited
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TrackingViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.allowsSelection = false
        let index = indexPath.row
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TrackTableViewCell
        cell.backgroundColor = .clear
        
        let key = Array(list.keys)[index] as String
        cell.lblTitle.text = "\(key)"
        
        guard let value = list[key] else{
            cell.lblDetail.text = "暫無資料"
            return cell
        }
       
        cell.lblDetail.text = "\(value!)"
        return cell
    }
    
    
}
