//
//  HistoryStatisticsViewController.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/06/19.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//

import UIKit
import Charts
class HistoryStatisticsViewController: UIViewController {

    @IBOutlet weak var lineChart: LineChartView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initialLineChart()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension HistoryStatisticsViewController{
    private func initialLineChart(){
        setLineChartVisual()
        drawChart()
    }
    private func setLineChartVisual(){
        lineChart.drawGridBackgroundEnabled = true
        lineChart.gridBackgroundColor = NSUIColor.black
        lineChart.borderColor = .red
        lineChart.noDataTextColor = .white
    }
}
// MARK: - LineChart Related
extension HistoryStatisticsViewController{
    private func drawChart(){
        let userdefault = UserDefaults.standard
        guard let recordData = userdefault.object(forKey: "threadViewedRecord") as? Data else{
            showlineCharError()
            initialThreadViewRecord()
            drawChart()
            return
        }
        
        do{
            var threadViewedRecord = try PropertyListDecoder().decode(Array<HistoryStatistics>.self, from: recordData)
            guard threadViewedRecord.count > 0 else {
                lineChart.noDataText += "資料不足"
                return
            }
            
            isUpdateTrackingData(&threadViewedRecord)
            setChartData(with: threadViewedRecord)
        }catch let err{
            print("🐼 \(err)")
        }
    }
    
    private func setChartData(with threadViewedRecord: [HistoryStatistics]){
        let dateData: [String] = {
            var date: [String] = []
            for data in threadViewedRecord{
                date.append(data.getDateWithForm())
            }
            return threadViewedRecord.map({ (d) in
                return d.getDateWithForm()
            })
        }()
        lineChart.backgroundColor = .white
        lineChart.xAxis.valueFormatter = IndexAxisValueFormatter(values: dateData)
        lineChart.xAxis.granularity = 1
        lineChart.animate(xAxisDuration: 1, yAxisDuration: 2)
        setChartEntries(withData: threadViewedRecord)
    }
    @discardableResult
    private func isUpdateTrackingData(_ array: inout [HistoryStatistics]) -> Bool{
        let last = array.last!
        let userDefault = UserDefaults.standard
        guard let threadVisited = userDefault.object(forKey: "threadVisited") as? Int else {
            return false
        }
        let current = HistoryStatistics(date: Date(), Count: threadVisited)
        
        if current.getDateWithForm() != last.getDateWithForm(){
            array.removeFirst()
            array.append(current)

            updateTrackingData(with: array)
            print("🐯 date diff, cur: \(current.getDateWithForm()), last: \(last.getDateWithForm())")
            return true
        }else if current.Count > last.Count{
            var last = array.last!
            last.Count = threadVisited
            array.removeLast()
            array.append(last)
            updateTrackingData(with: array)
            print("🐯 count diff")
            return true
        }
        
        
        
        print("🐯 no diff")
        return false
    }
    
    private func updateTrackingData(with array: [HistoryStatistics]){
        do{
            let data = try PropertyListEncoder().encode(array)
            let userdefault = UserDefaults.standard
            userdefault.set(data, forKey: "threadViewedRecord")
        }catch let err{
            print("🐼 \(err)")
        }
    }
    private func setChartEntries(withData historyData: [HistoryStatistics]){
        var dataEntries:[BarChartDataEntry] = []
        for index in 0..<historyData.count{
            let entry = BarChartDataEntry(x: Double(index), y: Double(historyData[index].Count))
            dataEntries.append(entry)
        }
        let dataSet = genDataSet(from: dataEntries)
        lineChart.data = LineChartData(dataSet: dataSet)
    }
    private func genDataSet(from dataEntries: [BarChartDataEntry]) -> LineChartDataSet{
        let dataSet = LineChartDataSet(values: dataEntries, label: "日期")
        dataSet.colors = ChartColorTemplates.colorful()
        dataSet.valueColors = [NSUIColor.green]
        dataSet.circleColors = dataSet.colors
        let gradientColors = [UIColor.cyan.cgColor, UIColor.clear.cgColor] as CFArray
        let colorLocations:[CGFloat] = [1.0, 0.0]
        let gradient = CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(),
                                       colors: gradientColors, locations: colorLocations)
        dataSet.fill = Fill.fillWithLinearGradient(gradient!, angle: 90.0)
        dataSet.highlightColor = .white
        dataSet.drawFilledEnabled = true
        return dataSet
    }
    private func showlineCharError(){
        let err = "Data no found!!"
        lineChart.noDataText = err
        alertPopUp(content: err)
    }
}
extension HistoryStatisticsViewController{
    private func initialThreadViewRecord(){
        let array = fillHistoryStatictis()
        assert(array.count > 0)
        
        do{
            let data = try PropertyListEncoder().encode(array)
            let userdefault = UserDefaults.standard
            userdefault.set(data, forKey: "threadViewedRecord")
        }catch let err{
            print("🐼 \(err)")
        }
    }
    private func fillHistoryStatictis() -> [HistoryStatistics]{
        var array: [HistoryStatistics] = []
        let maxEntity = 5
        let currentDate = Date()
        assert(maxEntity > 0)
        
        guard let numThreadVisited = getThreadVisitedCount() else{ return array }
        for _ in 0..<maxEntity{
            let fakeData = HistoryStatistics(date: currentDate, Count: numThreadVisited)
            array.append(fakeData)
        }
        return array
    }
    
    private func getThreadVisitedCount() -> Int? {
        let userdefault = UserDefaults.standard
        guard let num = userdefault.object(forKey: "threadVisited") as? Int else{
            return nil
        }
        return num
    }
}
