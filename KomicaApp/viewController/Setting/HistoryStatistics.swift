//
//  HistoryStatistics.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/05/29.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//

import Foundation
import UIKit


struct HistoryStatistics: Codable {
    var date: Date
    var Count: Int
    func getCountWithDouble() -> Double{
        return Double(self.Count)
    }
    func getDateWithForm(format : String = "dd-MM") -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self.date)
    }
}
