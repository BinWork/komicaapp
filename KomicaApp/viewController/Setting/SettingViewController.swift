//
//  SettingViewController.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/01/11.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//

import UIKit
import Charts

class SettingViewController: UIViewController {
    @IBOutlet weak var lineChart: LineChartView!
    @IBOutlet weak var segMenu: UISegmentedControl!
    /// 在Komica跟Komica2之間切換
    @IBAction func actionSegMenu(_ sender: UISegmentedControl) {
        Vibration(.light)
        swapMenuSource(withIndex: sender.selectedSegmentIndex)
    }
    /// 清除App的所有Cache
    @IBAction func actionClearAllCache(_ sender: UIButton) {
        Vibration(.light)
        clearDiskCache()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        segMenu.selectedSegmentIndex = 0
        
    }
    

}


extension SettingViewController{
    /// 交換首頁
    private func swapMenuSource(withIndex index: Int){
        let userDefaults = UserDefaults.standard
        var isTrigger = false
        switch index{
        case 0:
            userDefaults.set("https://komica.org/m/", forKey: "webMenu")
            isTrigger = false
        case 1:
            userDefaults.set("https://komica2.net/m/", forKey: "webMenu")
            isTrigger = true
        default:    break
        }
        let url = userDefaults.string(forKey: "webMenu")!
        arraySectionAll.removeAll()
        Parsing().requestMenuHttp(urlString: url)
        DispatchQueue.main.async {[weak self] in
            self?.alertPopUp(content: "模式: \(isTrigger ? "上車" : "一般")")
        }
    }
}
extension SettingViewController{
    /// 清除所有Disk上的所有Cache
    /**
     func clearDiskCache() {
     let fileManager = FileManager.default
     let myDocuments = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
     let diskCacheStorageBaseUrl = myDocuments.appendingPathComponent("diskCache")
     guard let filePaths = try? fileManager.contentsOfDirectory(at: diskCacheStorageBaseUrl, includingPropertiesForKeys: nil, options: []) else { return }
     for filePath in filePaths {
     print("cache path: \(filePath)")
     try? fileManager.removeItem(at: filePath)
     }
     }
     **/
    /// clearCache-方法二
    private func clearDiskCache() {
        let fileManager = FileManager.default
        let tempFolderPath = NSTemporaryDirectory()
        do {
            let filePaths = try fileManager.contentsOfDirectory(atPath: tempFolderPath)
            if filePaths.isEmpty{
                print("no caches path is found! :)")
            }else{
                /// clear all
                for filePath in filePaths {
                    print("cache path: \(filePath)")
                    try fileManager.removeItem(atPath: tempFolderPath + filePath)
                }
            }
        } catch let error as NSError{
            print("Could not clear temp folder: \(error)")
        }
    }
}
