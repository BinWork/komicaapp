//
//  DebugUserDefaultViewController.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/02/27.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//

import UIKit
class DebugUserDefaultCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblTitle.textColor = tintColor
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
class DebugUserDefaultViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var debugTableView: UITableView!
    
    let UserDictionary = UserDefaults.standard.dictionaryRepresentation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Total Item: " + String(UserDictionary.count)
        
        lblTitle.text = "Total Item: " + String(UserDictionary.count)
        //  tableview的cell高度自適應
        debugTableView.rowHeight = UITableView.automaticDimension
        debugTableView.estimatedRowHeight = UITableView.automaticDimension
        debugTableView.flashScrollIndicators()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DebugUserDefaultViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UserDictionary.count
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.allowsSelection = false
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DebugUserDefaultCell
        let index = indexPath.row
        let key = Array(UserDictionary.keys)[index] as String
        cell.lblTitle.text = "\(key)"

        guard let value = UserDictionary[key] else{
            cell.lblValue.text = "暫無資料"
            return cell
        }
        
        cell.lblValue.text = "\(value)"
        return cell
    }
    
    
}
