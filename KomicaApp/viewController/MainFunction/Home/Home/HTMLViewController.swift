//
//  HTMLViewController.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/01/26.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//

import UIKit

class HTMLViewController: UIViewController {

    @IBOutlet weak var txtView: UITextView!
    var responseHTML:String?
    var webTitle:String?
    private var numWord:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshUI()
        printHtml()
    }
    func refreshUI(){
        self.navigationItem.title = self.webTitle
        showWordTotal()
    }
    func printHtml(){
        txtView.text = responseHTML
        txtView.setContentOffset(.zero, animated: false)
        responseHTML = nil
    }
    func showWordTotal(){
        if responseHTML != nil{
            DispatchQueue.main.asyncAfter(deadline: .now()+0.3, execute: {
                if let byte = self.responseHTML?.lengthOfBytes(using: String.defaultCStringEncoding){
                 self.alertPopUp(content: "共\(byte)字")
                }
            })
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        responseHTML = nil
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
