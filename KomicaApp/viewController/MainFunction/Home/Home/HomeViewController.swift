//
//  HomeViewController.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2018/12/25.
//  Copyright © 2018 nwfmbin2. All rights reserved.
//

import UIKit
import Alamofire
import UserNotifications
class HomeCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgAvailable: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        accessoryType = .disclosureIndicator
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected{
            lblTitle.textColor = .black
            backgroundColor = .lightGray
        }else{
            lblTitle.textColor = .lightGray
            backgroundColor = .clear
        }
        
        tintColor = .clear

        // Configure the view for the selected state
    }
}
class HomeViewController: UIViewController {
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var menuSearchBar: UISearchBar!
    /// 判斷searchBar是否在搜尋中
    private var isSearching:Bool = false
    /// pull刷新的object
    private let refreshControl = UIRefreshControl()
    /// 儲存searchBar結果的Array
    lazy var arraySearchBar:[SectionMenu] = []
    /// 儲存searhBar搜尋版面「名稱」的結果合集
    lazy var arraySearchBar_Title:[SectionMenu] = []
    /// 儲存searhBar搜尋版面「網址」的結果合集
    lazy var arraySearchBar_Website:[SectionMenu] = []
    /// 儲存目前可正常解析的版面
    lazy var arraySectionAvailable:[SectionMenu] = []
    /// 存放網頁爬下來的原始碼
    lazy var contentHTML:String? = ""
    /// 選擇顯示「所有版面」or「目前可解析的版面」
    lazy var isAvailable:Bool = true
    @IBOutlet weak var itemAvailable: UIBarButtonItem!
    
    @IBAction func showAvailable(_ sender: UIBarButtonItem) {
        itemAvailable.title = isAvailable ? "All" : "Available"
//        isAvailable = isAvailable ? false : true
        menuTableView.setContentOffset(.zero, animated: false)
        menuTableView.AnimationReloadData()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.menuSearchBar.text = nil
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        if menuSearchBar.isFirstResponder{
            menuSearchBar.endEditing(true)
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        isSearching = false
        menuTableView.isUserInteractionEnabled = true
        menuTableView.reloadData()
    }
    /// 當無法解析一個版面時呼叫此@objc恢復tableView的isUserInteractionEnabled
    @objc func recoverMenuIsUserInteractionEnabled(){
        menuTableView.isUserInteractionEnabled = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //  checkIf3DtouchSupported()
        refreshUI()
        requestMenuPage()
        notificationObserver()
    }
    
    /// 刷新頁面
    func refreshUI(){
        navigationItem.title = "Komica App Demo"
        menuTableView.isHidden = true
        loadRefreshControl()
    }
    /// 送出request請求首頁資訊
    func requestMenuPage(){
        let menuWeb:String = getMenuURL()
        Parsing().requestMenuHttp(urlString: menuWeb)
    }
    /// 監聽notification
    func notificationObserver(){
        let notification = NotificationCenter.default
        notification.addObserver(self, selector: #selector(reloadMenu),
                                 name: Notification.Name("reloadMenu"), object: nil)
        notification.addObserver(self, selector: #selector(loadSection),
                                 name: Notification.Name("loadSection"), object: nil)
        notification.addObserver(self, selector: #selector(recoverMenuIsUserInteractionEnabled),
                                 name: Notification.Name("recoverMenuIsUserInteractionEnabled"),
                                 object: nil)
    }
    
    /// 將可解析版面放入arraySectionAailable裡 然後重整tableView
    @objc func reloadMenu(){
        for web in arraySectionAll{
            for availableWeb in arrayAvailableWeb{
                if web.website.contains(availableWeb){
                    arraySectionAvailable.append(web)
                }
            }
        }
        menuTableView.isHidden = false
        menuTableView.AnimationReloadData()
    }
    
    /**
     * 手動重整
     */
    @objc func refresh(_ sender: Any) {
        refreshControl.attributedTitle = NSAttributedString(string: "")
        menuTableView.AnimationReloadData()
        refreshControl.endRefreshing()
    }
    
    /// 觸發Segue載入SectionViewController
    @objc func loadSection(){
        if let indexPath = menuTableView.indexPathForSelectedRow {
            menuTableView.deselectRow(at: indexPath, animated: true)
        }
        performSegue(withIdentifier: "showSection", sender: self)
    }


    /// 在tableView載入refreshControl
    func loadRefreshControl(){
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        menuTableView.addSubview(refreshControl)
    }
    
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        navigationItem.backBarButtonItem?.title = "首頁"
        if segue.identifier == "showSection"{
            let desVc = segue.destination as! SectionViewController
            desVc.strTitle = SegueData.title
            desVc.strWebsite = SegueData.website
        }
        if segue.identifier == "debugHTMLSegue"{
            let desVc = segue.destination as! HTMLViewController
            desVc.responseHTML = contentHTML
            desVc.webTitle = SegueData.title
            contentHTML = nil
        }
        let backItem = UIBarButtonItem()
        backItem.title = "首頁"
        navigationItem.backBarButtonItem = backItem 
    }
    
    /**
     爬指定網址的HTML原始碼
     - parameter urlString: 網址
     */
    func parsingHTML(urlString:String){
        let url = NSURL(string: urlString)
        let task = URLSession.shared.dataTask(with: url! as URL) {
            [weak self] (data, response, error) in
            
            guard let strongSelf = self else { return }
            
            guard error == nil else {
                strongSelf.alertPopUp(content: "請求失敗")
                return
            }
            
            let urlContent = NSString(data: data!,
                                      encoding: String.Encoding.utf8.rawValue)
            strongSelf.contentHTML = urlContent! as String
            
            DispatchQueue.main.async {
                strongSelf.performSegue(withIdentifier: "debugHTMLSegue", sender: self)
            }
        }
        task.resume()
        
    }
    deinit {
        print("🐯 \(#file) deinit.")
    }
}


























extension HomeViewController{
    func checkIf3DtouchSupported(){
        if traitCollection.forceTouchCapability == .available{
            registerForPreviewing(with: self, sourceView: menuTableView)
        }
    }
    func getMenuURL() -> String{
        /***
        guard let webMenu = UserDefaults.standard.string(forKey: "webMenu") else{
            print("🐼 Default webMenu no found!")
            UserDefaults.standard.set("https://komica.org/m/", forKey: "webMenu")
            return "https://komica.org/m/"
        }
        return webMenu
        ***/
        return "https://komica.org/m/"
    }
}
extension HomeViewController: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = isSearching ?
                    arraySearchBar.count :
                    (isAvailable ? arraySectionAvailable.count : arraySectionAll.count)
        return count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.tableFooterView = UIView(frame: .zero)
        let menuCell = tableView.dequeueReusableCell(withIdentifier: "menuCell") as! HomeCell
        var element:SectionMenu?
        var txtTitle:String = ""
        var web:String = ""
        if isAvailable{
            element = arraySectionAvailable[indexPath.row]
        }else{
            element = arraySectionAll[indexPath.row]
        }
        
        
        if isSearching{
            var context = arraySearchBar[indexPath.row].title
            if arraySearchBar_Website.count > 0{
                context += " ( \(arraySearchBar[indexPath.row].website) )"
            }
            txtTitle = context
            web = arraySearchBar[indexPath.row].website
        }else{
            txtTitle =  element!.title
            web = element!.website
        }

        var colorAvailable:UIColor = UIColor.red
        for item in arrayAvailableWeb{
            if web.contains(item){
                colorAvailable = UIColor.green
                break
            }
        }
        menuCell.lblTitle.text = txtTitle
        menuCell.imgAvailable.layer.cornerRadius = menuCell.imgAvailable.frame.size.height/2
        menuCell.imgAvailable.backgroundColor = colorAvailable
        return menuCell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Vibration(.light)
        
        tableView.isUserInteractionEnabled = false
        crawler(indexPath)
        menuSearchBar.endEditing(true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    /// 對KomicaAPi發送請求
    func genRequestKomicaAPI(_ indexPath: IndexPath){
        let index = indexPath.row
        var element:SectionMenu?
        if isSearching{
            element = arraySearchBar[index]
        }else{
            if isAvailable{
                element = arraySectionAvailable[index]
            }else{
                element = arraySectionAll[index]
            }
        }
        let url = (element?.website)!
        newAllThreadsGroup?.enter()
        requestAPI.request(url: url, object: .allThreads)
        newAllThreadsGroup?.notify(queue: .main, execute: {
            self.loadSection()
            newAllThreadsGroup = nil
        })
        DispatchQueue.main.async { [weak self] in
            self?.menuTableView.isUserInteractionEnabled = true
        }
    }
    /// 爬蟲
    func crawler(_ indexPath: IndexPath){
        //  清空文字暫存
        arrayThread.removeAll()
        menuSearchBar.endEditing(true)
        let index = indexPath.row
        
        var element: SectionMenu?
        
        if isSearching{
            element = arraySearchBar[index]
        }else{
            if isAvailable{
                element = arraySectionAvailable[index]
            }else{
                element = arraySectionAll[index]
            }
        }
        let title = (element?.title)!
        let website = (element?.website)!
        //  準備segue的資料
        SegueData = SectionMenu(title: title, website: website)
        //  爬選擇版面
        let parsing = Parsing()
        newAllThreadsGroup = DispatchGroup()
        newAllThreadsGroup?.enter()
        parsing.requestThreadHttp(html: website,typeParsing: .Section)
        newAllThreadsGroup?.notify(queue: .main, execute: {
            self.loadSection()
            newAllThreadsGroup = nil
        })
    }
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        //  瀏覽網頁原始碼
        let viewHTMLSource: UIContextualAction = {
            let action = UIContextualAction(style: .normal, title: nil, handler: {(action,view,handler) in
                let index = indexPath.row
                let url = self.isAvailable ? self.arraySectionAvailable[index].website :
                    arraySectionAll[index].website
                let title = self.isAvailable ? self.arraySectionAvailable[index].title :
                    arraySectionAll[index].title
                SegueData = SectionMenu(title:title, website:url)
                self.parsingHTML(urlString: SegueData.website)
                handler(true)
            })
            return action
        }()
        viewHTMLSource.image = UIImage(named:"icon_Link")
        viewHTMLSource.backgroundColor = UIColor(displayP3Red: 30/255,green: 30/255,
                                                 blue: 30/255, alpha: 1.0)
        return UISwipeActionsConfiguration(actions: [viewHTMLSource])
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}
extension HomeViewController: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if isAvailable{
            arraySearchBar_Title = arraySectionAvailable.filter({$0.title.lowercased().contains(searchText.lowercased())})
            arraySearchBar_Website = arraySectionAvailable.filter({$0.website.lowercased().contains(searchText.lowercased())})
        }else{
            arraySearchBar_Title = arraySectionAll.filter({$0.title.lowercased().contains(searchText.lowercased())})
            arraySearchBar_Website = arraySectionAll.filter({$0.website.lowercased().contains(searchText.lowercased())})
        }
        arraySearchBar = arraySearchBar_Title + arraySearchBar_Website

        //  如果搜尋bar字串為留空狀態 則設isSearching為false
        isSearching = searchText == "" ? false : true
        menuTableView.reloadData()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        searchBar.endEditing(true)
        return true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        searchBar.text = ""
        searchBar.endEditing(true)
    }

}
extension HomeViewController: UIViewControllerPreviewingDelegate{
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        if let selectedRow = menuTableView.indexPathForRow(at: location)?.row{
            SegueData = arraySectionAll[selectedRow]
        }
        return storyboard?.instantiateViewController(withIdentifier: "infoMenuView")
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        if let infoMenuView = storyboard?.instantiateViewController(withIdentifier: "infoMenuView"){
            self.present(infoMenuView,animated: true,completion: nil)
        }
    }
    
}
