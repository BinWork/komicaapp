//
//  ImgSourceViewController.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/01/05.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//

import UIKit
import FLAnimatedImage
class ImgSourceViewController: UIViewController {

    @IBOutlet weak var imgSource: UIImageView!
    @IBOutlet weak var scrollImgView: UIScrollView!
    
    var imageNode: komicaImageNode!
    var titleImage = String()
    /*********************
     *********************/
    lazy var viewDrawing:DrawingView = {
        let drawing = DrawingView()
        drawing.frame = view.bounds
        drawing.backgroundColor = .clear
        return drawing
    }()
    @IBOutlet weak var barItemDraw: UIBarButtonItem!
    @IBAction func actionAddDrawingView(_ sender: UIBarButtonItem) {
        if viewDrawing.isDescendant(of: view ) == false{
            view.addSubview(viewDrawing)
            navigationItem.title = "繪圖模式"
            barItemDraw.title = "Quit"
            alertPopUp(content: "繪圖中")
        }else{
            
            guard viewDrawing.lines.count > 0 else{
                quitDrawingMode()
                return
            }
            popExitWarningAlert()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        initialScrollView()
        loadImageSource()
        initialTapFunction()
        // Do any additional setup after loading the view.
    }
    func initialScrollView(){
        scrollImgView.isUserInteractionEnabled = true
        scrollImgView.minimumZoomScale = 1.0
        scrollImgView.maximumZoomScale = 10.0
        
    }
    func loadImageSource(){
        loadImageFromUrl()
    }
    func loadImageFromUrl(){
        guard let urlSource = URL(string: imageNode.src)else{
            print("🐼 strOfSource is not valid! url:\(imageNode.src)")
            return
        }
        navigationItem.title = titleImage
        detectImageType(urlSource)
    }
    func detectImageType(_ source: URL){
        //  顯示原圖
        let imgPlaceHolder = UIImageView()
        
        imgPlaceHolder.sd_setImage(with: URL(string:imageNode.thumb), placeholderImage: nil)
        
        
        /// progressBar here
        print("asdf: \(view.bounds.size.height/2)")
        let frame = CGRect(x: 0.0, y: view.bounds.size.height/2,
                           width: view.bounds.size.width,
                           height: 300)
        let progressBar = UIProgressView(frame: frame)
        progressBar.backgroundColor = .red
        progressBar.tintColor = UIApplication.shared.keyWindow?.tintColor
        progressBar.trackTintColor = .lightGray
        view.addSubview(progressBar)
        
        
        switch imageNode.ext {
        case ".jpg", ".png":
            imgSource?.sd_setImage(with: source, placeholderImage: imgPlaceHolder.image, progress: { (receivedSize, expectedSize
                , url) in
                DispatchQueue.main.async {
                    progressBar.progress = Float(receivedSize)/Float(expectedSize)
                    progressBar.setNeedsLayout()
                    print(progressBar.progress)
                    if receivedSize >= expectedSize{
                        print("finish download Image.")
                        UIView.animate(withDuration: 2.0, animations: {
                            progressBar.setNeedsLayout()
                            progressBar.alpha = 0.0
                        }, completion: { finish in
                            progressBar.removeFromSuperview()
                        })
                    }
                }
            }, completed: {(image, error, cacheTyped, url) in
                switch cacheTyped{
                case .disk, .memory :
                    print("Already downloaded this image before.")
                    progressBar.layoutIfNeeded()
                    progressBar.progress = 1.0
                    progressBar.removeFromSuperview()
                @unknown default:
                    break
                }
            })
        case ".gif":
            DispatchQueue.global().async { [weak self] in
                do {
                    let gifData = try Data(contentsOf: source)
                    DispatchQueue.main.async {
                        let animateImageView = FLAnimatedImageView(frame: (self?.imgSource!.frame)!)
                        animateImageView.contentMode = .scaleAspectFit
                        animateImageView.animatedImage = FLAnimatedImage(gifData: gifData)
                        self?.imgSource.addSubview(animateImageView)
                    }
                } catch {
                    print(error)
                }
            }
        default:
            imgSource?.sd_setImage(with: URL(string:imageNode.thumb), placeholderImage: imgPlaceHolder.image)
        }
    }
    /// 退出繪圖模式
    func quitDrawingMode(){
        viewDrawing.lines.removeAll()
        viewDrawing.setNeedsDisplay()
        viewDrawing.removeFromSuperview()
        navigationItem.title = titleImage
        barItemDraw.title = "Draw"
    }
    /// 彈出視窗詢問是否儲存塗鴉後的螢幕截圖
    func popExitWarningAlert(){
        let alertView = UIAlertController(title: "是否儲存圖片?",
                                          message: nil, preferredStyle: .alert)
        let actionSave = UIAlertAction(title: "儲存圖片", style: .default, handler: {
            [weak self] action in
            guard let strongSelf = self else { return }
            let imgScreenShot = strongSelf.getScreenShot()
            strongSelf.saveImageToAlbum(imgScreenShot)
            strongSelf.quitDrawingMode()
        })
        let actionCancel = UIAlertAction(title: "不儲存並退出", style: .cancel, handler: {
            [weak self] action in
            guard let strongSelf = self else { return }
            strongSelf.quitDrawingMode()
            strongSelf.navigationController?.popViewController(animated: true)
        })
        alertView.addAction(actionSave)
        alertView.addAction(actionCancel)
        present(alertView,animated: true,completion: nil)
    }
    
    /// 儲存圖片
    func getScreenShot() -> UIImage? {

        UIGraphicsBeginImageContextWithOptions(imgSource.frame.size, true, 2.0)
        guard let cgContext = UIGraphicsGetCurrentContext() else {
            print("Fail to get CGContext")
            return nil
        }
        view.layer.render(in: cgContext)
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else {
            print("Fail to get Image from current image context")
            return nil
        }
        UIGraphicsEndImageContext()
        let imageView = UIImageView()
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        imageView.frame = imgSource.frame
        return imageView.image
    }
    
}

//
//  ScrollView related
//
extension ImgSourceViewController:UIScrollViewDelegate{
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imgSource
    }
}
//
//  Gesture related
//
extension ImgSourceViewController{
    func initialTapFunction(){
    
        view.isUserInteractionEnabled = true
        imgSource.isUserInteractionEnabled = true
        holdAndSaveImg()
        rotateImg()
        zoomImg()
        dragDownDimiss()
    }
    //  action.旋轉圖片
    func rotateImg(){
        let rotateGesture = UIRotationGestureRecognizer(target: self, action: #selector(actionRotateImg))
        imgSource.addGestureRecognizer(rotateGesture)
    }
    @objc func actionRotateImg(_ sender:UIRotationGestureRecognizer){
        sender.view?.transform = (sender.view?.transform)!.rotated(by: sender.rotation)
        sender.rotation = 0.0
    }
    @objc func goBack(){
        Vibration(.light)
        navigationController?.popViewController(animated: true)
    }
    //  zoomIn
    func zoomImg(){
        let zoomGesture = UITapGestureRecognizer(target: self, action: #selector(zoomSpecify))
        zoomGesture.numberOfTapsRequired = 2
        imgSource.addGestureRecognizer(zoomGesture)
    }
    @objc func zoomSpecify(_ gestureRecognizer: UITapGestureRecognizer) {
        let targetScale = scrollImgView.zoomScale * 2.5
        let scale = min(targetScale, scrollImgView.maximumZoomScale/4)
        
        if scale > scrollImgView.zoomScale {
            let point = gestureRecognizer.location(in: imgSource)
            
            let scrollSize = scrollImgView.frame.size
            let size = CGSize(width: scrollSize.width / scale,
                              height: scrollSize.height / scale)
            let origin = CGPoint(x: point.x - size.width / 2,
                                 y: point.y - size.height / 2)
            scrollImgView.zoom(to:CGRect(origin: origin, size: size), animated: true)
        }else{
            scrollImgView.zoom(to: imgSource.frame, animated: true)
        }
    }
    //  action.儲存圖片
    func holdAndSaveImg(){
        let holdTap = UILongPressGestureRecognizer(target: self, action: #selector(alertSaveImage))
        holdTap.minimumPressDuration = 0.4
        view.addGestureRecognizer(holdTap)
    }
    @objc func alertSaveImage(_ sender: UIGestureRecognizer){
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let saveImage = UIAlertAction(title: "Save to Album", style: .default, handler:{
            [weak self] action in
            guard let strongSelf = self else { return }
            strongSelf.saveImageToAlbum(strongSelf.imgSource.image)
        })
        alert.addAction(cancel)
        alert.addAction(saveImage)
        present(alert,animated: true,completion: nil)
    }
    //  儲存圖片
    func saveImageToAlbum(_ image: UIImage?){
        guard image != nil else{
            alertPopUp(content: "Image not found!")
            return
        }
        UIImageWriteToSavedPhotosAlbum(image!, self, nil, nil)
        DispatchQueue.main.async {
            [weak self] in
            guard let strongSelf = self else { return }
            if var imgSavedCount = UserDefaults.standard.object(forKey: "imgSavedCount") as? Int {
                imgSavedCount += 1
                UserDefaults.standard.set(imgSavedCount, forKey: "imgSavedCount")
            }else{
                UserDefaults.standard.set(1, forKey: "imgSavedCount")
            }
            
            strongSelf.Vibration(.light)
            strongSelf.alertPopUp(content: "Saved")
        }
    }
    //  拖曳下拉取消ViewController
    private func dragDownDimiss(){
        let dragDismiss = UIPanGestureRecognizer(target: self, action: #selector(actionDragDismiss))
        imgSource.addGestureRecognizer(dragDismiss)
    }
    @objc func actionDragDismiss(_ sender: UIPanGestureRecognizer){
        let translation = sender.translation(in: view)
        view.frame.origin = translation
        if sender.state == .ended {
            let velocity = sender.velocity(in: view)
            if velocity.y > 1500 || velocity.y < -1500 || velocity.x > 1500{
                scrollImgView.backgroundColor = .clear
                view.backgroundColor = .clear
                UIView.animate(withDuration: 0.3, animations: {
                    self.imgSource.alpha = 0.0
                })
                navigationController?.popViewController(animated: true)
            }else{
                UIView.animate(withDuration: 0.3, animations: {
                    [weak self] in
                    guard let strongSelf = self else { return }
                    strongSelf.view.frame.origin = CGPoint.zero
                })
            }
        }
    }
}
