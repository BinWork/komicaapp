//
//  UIDrawingView.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/01/26.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//

import UIKit

class Line {
    var start: CGPoint
    var end: CGPoint
    init(_ start: CGPoint, _ end:CGPoint){
        self.start = start
        self.end = end
    }
}
class DrawingView: UIView{
    var lines: [Line] = []
    var lastPoint: CGPoint!
    var drawColor = UIColor.black
    override func didMoveToSuperview() {
        let btn = UIButton(frame: CGRect(x: frame.midX - 25, y: frame.maxY - 100, width: 50, height: 30))
        btn.setTitle("Clear", for: .normal)
        btn.titleLabel?.textAlignment = .center
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        btn.backgroundColor = .clear
        btn.alpha = 0.8
        btn.addTarget(self, action: #selector(clearAllLines), for: .touchUpInside)
        addSubview(btn)
    }

    @objc func clearAllLines(){
        lines.removeAll()
        setNeedsDisplay()
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        lastPoint = touch?.location(in: self)
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let newPoint = touch?.location(in: self)
        lines.append(Line(lastPoint, newPoint!))
        lastPoint = newPoint
        
        setNeedsDisplay()
    }

    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        for line in lines{
            let startPoint = CGPoint(x: line.start.x, y: line.start.y)
            context?.move(to: startPoint)
            let endPoint = CGPoint(x: line.end.x, y: line.end.y)
            context?.addLine(to: endPoint)
        }
        context?.setLineCap(CGLineCap.round)
        context?.setStrokeColor(UIColor.red.cgColor)
        context?.setLineWidth(5.0)
        context?.strokePath()
    }

}
