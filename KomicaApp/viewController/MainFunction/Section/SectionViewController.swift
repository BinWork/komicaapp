//
//  SectionViewController.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/01/05.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//

import UIKit
import SDWebImage
import SafariServices
internal class ThreadCell: UITableViewCell {
    /// 內文
    @IBOutlet weak var lblContent: UILabel!
    /// 貼文日期
    @IBOutlet weak var lblDate: UILabel!
    /// 貼文時間
    @IBOutlet weak var lblTime: UILabel!
    /// 貼文者ID
    @IBOutlet weak var lblUniqueID: UILabel!
    /// 貼文流水號
    @IBOutlet weak var lblSerialNo: UILabel!
    /// 貼文下被隱藏的回文
    @IBOutlet weak var lblIgnore: UILabel!
    /// 附圖
    @IBOutlet weak var imgThumbnail: UIImageView!
    /// 附圖寬度
    @IBOutlet weak var imgThumbnailWidth: NSLayoutConstraint!
    /// 附圖至左側的間隔
    @IBOutlet weak var imgThumbnailLeading: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
        lblContent.textColor = UIColor(displayP3Red: 0.8, green: 0.8, blue: 0.8, alpha: 1.0)
        
        lblSerialNo.textColor = .white
        lblSerialNo.adjustsFontSizeToFitWidth = true
        lblSerialNo.font = UIFont.systemFont(ofSize: 10)
        lblSerialNo.minimumScaleFactor = 0.5
        
        lblTime.adjustsFontSizeToFitWidth = true
        lblTime.minimumScaleFactor = 0.5
        lblTime.textColor = .white
        
        
        lblDate.textColor = .white
        tintColor = .clear
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
/// for 回到「版面頂端」功能
internal class BackToTopCell: UITableViewCell{
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

/// for 頁數pickerView
internal class ThreadPagingCell: UITableViewCell{
    @IBOutlet weak var txtFieldPaging: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        txtFieldPaging.tag = 500
        txtFieldPaging.textAlignment = .center
        txtFieldPaging.isSelected = false
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
internal class SectionViewController: UIViewController {
    deinit {
        print("🐷 SectionViewController Deinit!")
        previousSection = ""
    }
    /// 目前版面的網址
    @IBOutlet weak var lblWebsite: UILabel!
    /// 版面tableView
    @IBOutlet weak var sectionTableView: UITableView!
    /// 使用safari打開網址並前往網頁
    @IBAction func actionCompose(_ sender: UIBarButtonItem) {
        Vibration(.light)
        showWebsiteWithSafari(url: strWebsite)
    }
    
    ///  版面名稱 from Home
    lazy var strTitle = String()
    ///  版面網址 from Home
    lazy var strWebsite = String()
    ///  儲存該版面的分頁用 (可往回翻幾頁)
    lazy var arrayPage:[String] = []
    ///  儲存欲把縮圖展開成原圖的imgSourceURL
    lazy var strImgSourceURL = String()
    /// 儲存準備跳轉頁面的網址
    private var strPaging: String?
    ///  pull刷新的object
    private let refreshControl = UIRefreshControl()
    /// 動態縮圖的透明背景layer
    lazy var layerPreviewBackground:UIVisualEffectView = {
        let layer = UIVisualEffectView.init(frame: view.bounds)
        let blurEffect = UIBlurEffect.init(style: UIBlurEffect.Style.dark)
        layer.backgroundColor = .clear
        layer.effect = blurEffect
        layer.alpha = 0.0
        return layer
    }()
    private let pagePickerView:UIPickerView = {
        let pickerview = UIPickerView()
        pickerview.isUserInteractionEnabled = true
        pickerview.backgroundColor = .clear
        pickerview.reloadAllComponents()
        pickerview.selectedRow(inComponent: 0)
        pickerview.tag = 500
        return pickerview
    }()
    /// toolbar
    private let pagePickerViewToolbar: UIToolbar = {
        let toolbar = UIToolbar()
        toolbar.barStyle = UIBarStyle.default
        toolbar.isTranslucent = true
        toolbar.sizeToFit()
        
        toolbar.alpha = 0.8
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(goSpecifyPage))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        toolbar.setItems([spaceButton, doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        return toolbar
    }()
    
    @objc func goSpecifyPage(){
        view.endEditing(true)
        if strPaging == nil{
            return
        }
        guard URL(string: strPaging!) != nil else{
            print("🐼 paging URL is not Valid: \(strPaging!)")
            return
        }
        //  清空文字暫存
        //  爬選擇版面
        let parsing = Parsing()
        newAllThreadsGroup = DispatchGroup()
        newAllThreadsGroup?.enter()
        parsing.requestThreadHttp(html: strPaging!,typeParsing: .Section)
        newAllThreadsGroup?.notify(queue: .main, execute: {
            [weak self] in
            let numWillRemoved = arrayThread.count / 2
            arrayThread.removeFirst(numWillRemoved)
            self?.sectionTableView.setContentOffset(.zero, animated: false)
            self?.sectionTableView.AnimationReloadData()
            newAllThreadsGroup = nil
        })
    }
    
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        previousSection = strWebsite
        initialStatus()
    }
    override func viewWillDisappear(_ animated: Bool) {
        if isMovingFromParent{
            Vibration(.light)
        }
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        if isMovingFromParent{
            SDImageCache.shared().clearMemory()
            SDImageCache.shared().clearDisk()
        }
        resetExpand()
    }
    
    private func resetExpand(){
        if arrayThread.count > 0{
            for index in 0...arrayThread.count - 1{
                arrayThread[index].isExpaned = false
            }
        }
        sectionTableView.reloadData()
    }
    
    
    /// 初始化一些設定
    private func initialStatus(){
        if let textField = view.viewWithTag(pagePickerView.tag) as? UITextField{
            let index = pagePickerView.selectedRow(inComponent: 0)
            textField.text = "第\(index)頁"
        }
        navigationItem.title = strTitle
        initialPickerView()
        
        
        lblWebsite.text = strWebsite
        lblWebsite.textColor = .lightGray
        //  refresh
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refreshCurrentSection), for: .valueChanged)
        sectionTableView.addSubview(refreshControl)
        
        //  tableview的cell高度自適應
        sectionTableView.rowHeight = UITableView.automaticDimension
        sectionTableView.estimatedRowHeight = 1500
        
        //  浮動刷新版面按鈕
        floatingAccessoryButton()
    }
    
    
    /// 設定pickerView在Navigation標題上
    func initialPickerView(){
        pagePickerView.delegate = self
        pagePickerView.dataSource = self
        
        let maxPage = 9
        for index in 0...maxPage - 1{
            arrayPage.append("\(index)")
        }
        pagePickerViewToolbar.tintColor = view.tintColor
        pagePickerViewToolbar.backgroundColor = pagePickerView.backgroundColor
        
    }

    /// 浮動按鈕物件
    private var floatBtn:UIImageView!
    /// 浮動按鈕功能
    func floatingAccessoryButton(){
        initialFloatingButton()
        view.addSubview(floatBtn)
    }
    
    /// 重整當前版面資料
    @objc func refreshCurrentSection(_ sender: Any) {
        view.isUserInteractionEnabled = false
        refreshControl.attributedTitle = NSAttributedString(string: "")
        let parsing = Parsing()
        newAllThreadsGroup = DispatchGroup()
        newAllThreadsGroup?.enter()
        arrayThread.removeAll()
        parsing.requestThreadHttp(html: (strPaging != nil ? strPaging! : strWebsite),typeParsing: .Section)
        newAllThreadsGroup?.notify(queue: .main, execute: {
            [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.sectionTableView.AnimationReloadData()
            strongSelf.refreshControl.endRefreshing()
            strongSelf.view.isUserInteractionEnabled = true
            newAllThreadsGroup = nil
        })
    }
    
    /// 用內建瀏覽器safari打開url
    private func openInSafari(linkOfWebsite link: String?){
        if let url = URL(string: link ?? ""){
            let vcSafari = SFSafariViewController(url: url)
            present(vcSafari, animated: true, completion: nil)
        }
    }
    
    /// 用瀏覽器打開網址
    private func showWebsiteWithSafari(url: String){
        let alertView = UIAlertController.init(title: "選擇網址打開方式", message: nil, preferredStyle: .actionSheet)
        let browerVC = UIAlertAction.init(title: "內建瀏覽器", style: .default, handler: {
            [weak self] action in
            self?.performSegue(withIdentifier: "showBrowser", sender: self)
        })
        browerVC.setValue(view.tintColor, forKey: "titleTextColor")
        
        let safariBrower = UIAlertAction.init(title: "Safari", style: .default, handler: {
            [weak self] action in
            self?.openInSafari(linkOfWebsite: url)
        })
        safariBrower.setValue(view.tintColor, forKey: "titleTextColor")
        
        let actionCancel = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
        actionCancel.setValue(view.tintColor, forKey: "titleTextColor")
        
        alertView.addAction(browerVC)
        alertView.addAction(safariBrower)
        alertView.addAction(actionCancel)
        present(alertView,animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let backButtonTitle = arrayKomicaPost.first?.info.title{
            navigationItem.backBarButtonItem?.title = backButtonTitle
        }
        if segue.identifier == "showSourceImage"{
            let desVc = segue.destination as! ImgSourceViewController
            desVc.navigationItem.backBarButtonItem?.title = previousSection
            
        }
        if segue.identifier == "showThread"{
            let desVc = segue.destination as! ThreadViewController
            desVc.strWeb = strWebsite
            desVc.arrayPost = arrayKomicaPost
            desVc.navigationItem.backBarButtonItem?.title = previousSection
        }
        if segue.identifier == "showBrowser"{
            let desVc = segue.destination as! BrowserViewController
            desVc.targetURL = strWebsite
            desVc.navigationItem.backBarButtonItem?.title = previousSection
        }
        let backItem = UIBarButtonItem()
        backItem.title = strTitle
        navigationItem.backBarButtonItem = backItem
    }
    
    

    
    
    
    
    
    

}
//
//  TableView related
//
extension SectionViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayThread.count + 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section < arrayThread.count{
            guard arrayThread[section].isExpaned else{
                return 1
            }
            return arrayThread[section].post.count
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section < arrayThread.count{
            if indexPath.row == 0 && arrayThread[indexPath.section].isExpaned == false{
                return 120
            }else{
                return UITableView.automaticDimension
            }
        }
        return UITableView.automaticDimension
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        switch section{
            case _ where section < arrayThread.count:
                var cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ThreadCell
                cell = setCell(cell: cell, indexPath: indexPath)
                return cell
            case _ where section == arrayThread.count:
                /// 「回到頂端」cell
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellBottom", for: indexPath) as! BackToTopCell
                return cell
            case _ where section == arrayThread.count + 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellPaging",for: indexPath) as! ThreadPagingCell
                cell.txtFieldPaging.inputView = pagePickerView
                cell.txtFieldPaging.inputAccessoryView = pagePickerViewToolbar
                return cell
            default:
                return UITableViewCell()
        }
        
    }
    func setCell(cell: ThreadCell, indexPath: IndexPath) -> ThreadCell{
        //  圖片
        if arrayThread[indexPath.section].post[indexPath.row].urlThumbnail != ""{
            cell.imgThumbnailWidth.constant = 80
            cell.imgThumbnail.sd_setImage(with: URL(string: arrayThread[indexPath.section].post[indexPath.row].urlThumbnail),
                                          placeholderImage: nil)
            if checkGestureAdded(cell, target: "imageSource") == false{
                let tap = UITapGestureRecognizer(target: self, action: #selector(previewImage))
                cell.imgThumbnail.isUserInteractionEnabled = true
                cell.imgThumbnail.addGestureRecognizer(tap)
            }
        }else{
            cell.imgThumbnailWidth.constant = 0
        }
        
        ///  區分原po和回文用
        if cell.gestureRecognizers != nil{
            cell.gestureRecognizers?.removeAll()
        }
        if indexPath.row == 0{
            cell.backgroundColor = .black
            cell.imgThumbnailLeading.constant = 0
            cell.lblContent.font = UIFont.systemFont(ofSize: 18)
        }else{
            cell.backgroundColor = UIColor(displayP3Red: 15/255, green: 15/255,
                                           blue: 15/255, alpha: 1.0)
            cell.imgThumbnailLeading.constant = 15
            cell.lblContent.font = UIFont.systemFont(ofSize: 16)
        }
        ///  取得貼文資訊
        let content = arrayThread[indexPath.section].post[indexPath.row].contentPost
        let detail = arrayThread[indexPath.section].post[indexPath.row].detailPost
        cell.lblDate.text = detail.date
        cell.lblTime.text = detail.time
        /// 判斷貼文是否重複出現
        if detail.id.total > 1{
            let total = detail.id.total
            let current = detail.id.current
            cell.lblUniqueID.text = detail.id.string + " (\(current!)/\(total!))"
            cell.lblUniqueID.textColor = detail.colorUniqueID
        }else{
            cell.lblUniqueID.text = detail.id.string
            cell.lblUniqueID.textColor = .white
        }
        
        cell.lblSerialNo.text = "No." + arrayThread[indexPath.section].post[indexPath.row].serialNo
        ///  判斷隱藏回文
        cell.lblIgnore.text = ""
        if indexPath.row == 0 && arrayThread[indexPath.section].isExpaned == false{
            let replyCount = arrayThread[indexPath.section].post.count
            if replyCount > 1{
                cell.lblIgnore.text = "( \(replyCount-1) 則新回應)"
                cell.lblIgnore.font = UIFont.boldSystemFont(ofSize: 10)
                cell.lblIgnore.textColor = .lightGray
                //  旋轉90‘
                //  cell.lblIgnore.transform = CGAffineTransform.init(rotationAngle: .pi/2)
            }
        }
        ///  判斷是否為展開模式
        cell.lblContent.lineBreakMode = .byWordWrapping
        if arrayThread[indexPath.section].isExpaned{
            cell.lblContent.numberOfLines = 0
        }else{
            cell.lblContent.numberOfLines = 4
            cell.lblContent.lineBreakMode = .byTruncatingTail
        }
        
        cell.lblContent.text = content
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = indexPath.section
        switch section {
        case _ where section < arrayThread.count:
            if indexPath.row  == 0 {
                Vibration(.light)
            }
            let section = indexPath.section
            arrayThread[section].isExpaned = arrayThread[section].isExpaned ? false : true
            let targetSection = IndexSet.init(integer: section)
            tableView.reloadSections(targetSection, with: .fade)
            storeHistory().storingHistory(arrayThread[section], type: .history)
        
        //  回到頂端
        case _ where section == arrayThread.count:
            Vibration(.light)
            contentBackToTop()
        //  頁數pickerView
        case _ where section == arrayThread.count + 1:
            print("pagingPickerView is touched.")
         default: break
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if indexPath.section < arrayThread.count{
            let section = indexPath.section
            let row = indexPath.row
            guard arrayThread[section].post[row].urlThumbnail != "" else{
                return UISwipeActionsConfiguration(actions: [])
            }
            //  新開原圖
            let openImgSource: UIContextualAction = {
                [weak self] in
                let action = UIContextualAction(style: .normal, title: nil, handler: {[weak self](action,view,handler) in
                    self?.strImgSourceURL = arrayThread[section].post[row].urlThumbnail
                    //self?.performSegue(withIdentifier: "showSourceImage", sender: self)
                    self?.alertPopUp(content: "暫時無法從版面打開原圖", Delay: 1, popUpDuration: 0.5, popOffDuration: 0.5)
                    handler(true)
                })
                return action
                }()
            openImgSource.image =  #imageLiteral(resourceName: "icon_Image")
            openImgSource.backgroundColor = tableView.cellForRow(at: indexPath)?.backgroundColor
            return UISwipeActionsConfiguration(actions: [openImgSource])
        }
        
        
        return UISwipeActionsConfiguration(actions: [])
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        if indexPath.section < arrayThread.count{
            if indexPath.row == 0 {
                //  展開全文
                let fullThread: UIContextualAction = {
                    [weak self] in
                    let action = UIContextualAction(style: .normal, title: nil, handler: {(action,view,handler) in
                        if arrayThread[indexPath.section].linkOrigin != nil{
                            self?.prepareLoadThread(indexPath: indexPath)
                            storeHistory().storingHistory(arrayThread[indexPath.section], type: .history)
                            handler(true)
                        }
                    })
                    return action
                    }()
                fullThread.image = #imageLiteral(resourceName: "icon_Detail")
                fullThread.backgroundColor = tableView.cellForRow(at: indexPath)?.backgroundColor
                return UISwipeActionsConfiguration(actions: [fullThread])
            }else{
                return UISwipeActionsConfiguration(actions: [])
            }
        }
        return UISwipeActionsConfiguration(actions: [])
    }
    //  展開全文
    func prepareLoadThread(indexPath: IndexPath){
        //let parsing = Parsing()
        //if let url  = arrayThread[indexPath.section].linkOrigin{
        //parsing.requestThreadHttp(html: url, typeParsing: .Thread)
        //}
        
        if let op = Int(arrayThread[indexPath.section].post[indexPath.row].serialNo){
            print("🐯 op:\(op), url:\(strWebsite)")
            newPostsGroup = DispatchGroup()
            newPostsGroup?.enter()
            requestAPI.request(url: strWebsite, object: .thread, limit: 50, op: op)
            newPostsGroup?.notify(queue: .main, execute: {
                [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.performSegue(withIdentifier: "showThread", sender: self)
                newPostsGroup = nil
            })
        }else{
            print("🐼 fail to get op")
        }
    }
    //  從Cell打開預覽圖
    @objc func previewImage(_ sender:UITapGestureRecognizer){
        let location = sender.location(in: sectionTableView)
        if let indexPath = sectionTableView.indexPathForRow(at: location){
            strImgSourceURL = arrayThread[indexPath.section].post[indexPath.row].urlThumbnail
            
            //  將該cell的圖片origin轉化成superview上的座標
            let cell = sectionTableView.cellForRow(at: indexPath) as! ThreadCell
            let imgFrameOfCell = cell.imgThumbnail.superview?.convert(cell.imgThumbnail.frame, to: sectionTableView.superview)
            cell.imgThumbnail.alpha = 0.0
            
            
            //  設定預覽圖ImageView
            let imgPreview:UIImageView = {
                //  imgPlaceHolder showed when imgPreview was not Valid
                let imgThumb = UIImageView()
                imgThumb.sd_setImage(with: URL(string:strImgSourceURL), placeholderImage: nil)
                //  the img which to previe
                let imgView = UIImageView()
                let strSource = convertThumbToSource(strImgSourceURL)
                let sourceURL = URL(string: strSource)
                imgView.sd_setImage(with: sourceURL, placeholderImage: imgThumb.image)
                imgView.contentMode = .scaleAspectFit
                imgView.frame = imgFrameOfCell!
                imgView.backgroundColor = .clear
                
                imgView.isUserInteractionEnabled = true
                
                ///  拖曳移除該ImageView
                let dragDismiss = CustomPanGestureRecognizer(target: self, action: #selector(actionDragDismiss))
                dragDismiss.indexPathImage = indexPath
                dragDismiss.rectImage = imgFrameOfCell
                imgView.addGestureRecognizer(dragDismiss)
                
                /// 長壓儲存圖片
                let longPress = UILongPressGestureRecognizer(target: self, action: #selector(alertSaveImage))
                longPress.minimumPressDuration = 0.4
                imgView.addGestureRecognizer(longPress)
                return imgView
            }()
            Vibration(.light)
            navigationController?.view.addSubview(layerPreviewBackground)
            navigationController?.view.addSubview(imgPreview)
            UIView.animate(withDuration: 0.3, animations: {
                [weak self] in
                self?.layerPreviewBackground.alpha = 0.8
                imgPreview.frame = UIScreen.main.bounds
                //  防治imgPreview在顯示中可以使用navigationController的swipeBack gesture功能
                self?.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
            })
        }
    }
    /**
     將附圖url從縮圖url還原成原圖url
     - parameter input: 縮圖的圖片網址
     - returns: 原圖url
    */
    func convertThumbToSource(_ input: String) -> String{
        var output = String()
        output = input.replacingOccurrences(of: "thumb", with: "src")
        output = output.replacingOccurrences(of: "s.png", with: ".png")
        output = output.replacingOccurrences(of: "s.jpg", with: ".jpg")
        output = output.replacingOccurrences(of: "s.gif", with: ".gif")
        return output
    }
    ///  拖曳下拉取消預覽圖
    @objc func actionDragDismiss(_ sender: CustomPanGestureRecognizer){
        let translation = sender.translation(in: view)
        sender.view?.frame.origin = translation
        if sender.state == .ended {
            let velocity = sender.velocity(in: view)
            let velocityLimit:CGFloat = 1500
            if velocity.y > velocityLimit || velocity.y < -velocityLimit ||
                velocity.x > velocityLimit || velocity.x < -velocityLimit{
                UIView.animate(withDuration: 0.3, animations: {
                    [weak self] in
                    sender.view?.frame = sender.rectImage!
                    self?.layerPreviewBackground.alpha = 0.0
                }, completion: { [weak self](finish:Bool) in
                    let cell = self?.sectionTableView.cellForRow(at: sender.indexPathImage!) as! ThreadCell
                    cell.imgThumbnail.alpha = 1.0
                    sender.view?.removeFromSuperview()
                    self?.layerPreviewBackground.removeFromSuperview()
                    
                    /// 回覆navigationController的swipeBack gesture功能
                    self?.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
                })
            }else{
                UIView.animate(withDuration: 0.3, animations: {
                    sender.view?.frame.origin = CGPoint.zero
                })
            }
        }
    }
    /// 警告視窗-儲存圖片
    @objc func alertSaveImage(_ sender: UIGestureRecognizer){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let saveImage = UIAlertAction(title: "Save to Album", style: .default, handler:{ [weak self] action in
            if let imgView = sender.view as? UIImageView{
                self?.saveImageToAlbum(imgView.image)
            }
        })
        alert.addAction(cancel)
        alert.addAction(saveImage)
        present(alert,animated: true,completion: nil)
    }
    
    /// 功能-儲存圖片
    func saveImageToAlbum(_ image: UIImage?){
        guard image != nil else{
            alertPopUp(content: "Image not found!")
            return
        }
        UIImageWriteToSavedPhotosAlbum(image!, self, nil, nil)
        DispatchQueue.main.async {
            [weak self] in
            if var imgSavedCount = UserDefaults.standard.object(forKey: "imgSavedCount") as? Int {
                imgSavedCount += 1
                UserDefaults.standard.set(imgSavedCount, forKey: "imgSavedCount")
            }else{
                UserDefaults.standard.set(1, forKey: "imgSavedCount")
            }
            
            self?.Vibration(.light)
            self?.alertPopUp(content: "Saved")
        }
    }
    ///  檢查gesture是否曾加入過
    func checkGestureAdded(_ sender:UITableViewCell,target:String) -> Bool{
        if let recognizers = sender.gestureRecognizers {
            for gesture in recognizers {
                switch target{
                case "imageSource":
                    if gesture is UITapGestureRecognizer {
                        return true
                    }
                default:
                    return false
                }
            }
        }
        return false
    }
    /// 讓tableView視野回到頂端
    @objc func contentBackToTop(){
        sectionTableView.setContentOffset(.zero, animated: true)
    }
    
}
//
//  PickerView related
//
extension SectionViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayPage.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        var pageWeb = strWebsite
        if pageWeb.contains("index.html"){
            pageWeb = pageWeb.replacingOccurrences(of: "index.html", with: "\(arrayPage[row]).html?")
        }else{
            pageWeb += arrayPage[row] + ".htm?"
        }
        if row == 0{
            strPaging = strWebsite
        }else{
            strPaging = pageWeb
        }
        if let textField = view.viewWithTag(pagePickerView.tag) as? UITextField{
            textField.text = "第\(arrayPage[row])頁"
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayPage[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 21)
        lbl.text = "第\(arrayPage[row])頁"
        lbl.textColor = .black
        lbl.textAlignment = .center

        return lbl
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        //  更改pickerView的文字顏色
        let text = arrayPage[row]
        let fontColor = UIColor(displayP3Red: 68/255, green: 116/255, blue: 185/255, alpha: 1)
        let stringKey:NSAttributedString.Key = NSAttributedString.Key.foregroundColor
        
        let font = NSAttributedString(string: text,
                                      attributes: [stringKey:fontColor])
        return font
    }
}


/// floating button related
extension SectionViewController{
    /// 對floatBtn做初始化動作
    func initialFloatingButton(){
        floatBtn = {
            let length:CGFloat = 50
            let customSize = CGSize(width: length, height: length)
            let customPoint = CGPoint(x: view.frame.maxX - 75 - length/2,
                                      y: view.frame.maxY - 100 - length/2)
            let customFrame = CGRect(origin: customPoint, size: customSize)
            let imgView = UIImageView(frame: customFrame)
            imgView.image = #imageLiteral(resourceName: "icon_App")
            imgView.layer.cornerRadius = length/2
            imgView.alpha = 0.7
            imgView.isUserInteractionEnabled = true
            //  滑動物件觸發
            let swipeUp = {
                return UISwipeGestureRecognizer(target: self,
                                                action: #selector(actionSwipe))
            }()
            swipeUp.direction = .up
            let swipeDown = {
                return UISwipeGestureRecognizer(target: self,
                                                action: #selector(actionSwipe))
            }()
            swipeDown.direction = .down
            let swipeRight = {
                return UISwipeGestureRecognizer(target: self,
                                                action: #selector(actionSwipe))
            }()
            swipeRight.direction = .right
            //  長壓物件觸發
            let press:UILongPressGestureRecognizer = {
                return UILongPressGestureRecognizer(target: self,
                                                    action: #selector(actionBackMenu))
            }()
            
            let drag:UIPanGestureRecognizer = {
                return UIPanGestureRecognizer(target: self,
                                              action: #selector(actionDragFloatBtn))
            }()
            press.require(toFail: drag)
            
            imgView.addGestureRecognizer(swipeUp)
            imgView.addGestureRecognizer(swipeDown)
            imgView.addGestureRecognizer(swipeRight)
            imgView.addGestureRecognizer(press)
            imgView.addGestureRecognizer(drag)
            return imgView
        }()
    }
    /// floatBtn - 返回首頁
    @objc func actionBackMenu(_ sender: UIGestureRecognizer){
        navigationController?.popViewController(animated: true)
    }
    /// floatBtn - 滑動
    @objc func actionSwipe(_ sender: UISwipeGestureRecognizer){
        sender.removeTarget(sender.view, action: nil)
        floatBtn.removeFromSuperview()
        switch sender.state {
        case .ended:
            switch sender.direction{
            case .up:
                let section = sectionTableView.numberOfSections - 1
                let row = 0
                let indexPath = IndexPath(row: row, section: section)
                sectionTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            case .down:
                let indexPath = IndexPath(row: 0, section: 0)
                sectionTableView.scrollToRow(at: indexPath, at: .top, animated: true)
            case .right:
                navigationController?.popViewController(animated: true)
            case .left: break
            default:    break
            }
            Vibration(.medium)
        default:    break
        }
        DispatchQueue.main.async {
            [weak self] in
            self?.view.addSubview((self?.floatBtn)!)
            sender.addTarget(self as Any, action: #selector(self?.actionSwipe))
        }
    }
    /// floatBtn - 拖曳
    @objc func actionDragFloatBtn(_ sender: UIPanGestureRecognizer){
        
        let translation = sender.translation(in: view)
        switch sender.state{
        case .began, .changed:
            sender.view?.transform = CGAffineTransform(translationX: translation.x, y: translation.y)
            
            print("floatBtn frame:\(floatBtn.frame)")
        default:
            UIView.animate(withDuration: 0.2, animations: {
                sender.view?.transform = CGAffineTransform.identity
            })
        }
        
    }
}
