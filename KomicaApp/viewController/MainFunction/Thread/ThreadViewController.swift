//
//  ThreadViewController.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/01/08.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//

import UIKit
import SDWebImage
import SafariServices



class ThreadViewController: UIViewController {
    deinit {
        previousThread = ""
    }
    /// 貼文網址
    @IBOutlet weak var lblWebsite: UILabel!
    /// 版面tableView
    @IBOutlet weak var threadTableView: UITableView!
    @IBAction func actionCompose(_ sender: UIBarButtonItem) {
        Vibration(.light)
        guard let postURL = requestAPI.currentSectionURL else{
            return
        }
        let url = "\(postURL)pixmicat.php?res=\(arrayPost.first!.info.id)"
        showWebsiteWithBrowser(url: url)
    }
    ///  版面網址 from Home
    lazy var strWeb = String()
    /// 儲存欲展開原圖的imgSourceURL
    lazy var strImgSourceURL = String()
    /// 圖片的data型態
    var dataImgSource:String?
    ///  pull刷新的object
    private let refreshControl = UIRefreshControl()
    static var oldThreadLink:String?
    /// 動態縮圖的透明背景layer
    lazy var layerPreviewBackground:UIVisualEffectView = {
        let layer = UIVisualEffectView.init(frame: view.bounds)
        let blurEffect = UIBlurEffect.init(style: UIBlurEffect.Style.dark)
        layer.backgroundColor = .clear
        layer.effect = blurEffect
        layer.alpha = 0.0
        return layer
    }()
    

    
    var arrayPost = [komicaPostNode]()
   
    override func viewWillDisappear(_ animated: Bool) {
        if isMovingFromParent{
            Vibration(.light)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initialStatus()
        //  浮動刷新版面按鈕
        floatingAccessoryButton()
    }
    private func initialStatus(){
        lblWebsite.textColor = .lightGray
        lblWebsite.text = strWeb
        navigationItem.title = "\(arrayKomicaPost.first!.info.title)"
        //  tableview的cell高度自適應
        threadTableView.rowHeight = UITableView.automaticDimension
        threadTableView.estimatedRowHeight = 500
        
        
        //  refresh
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refreshCurrentThread), for: .valueChanged)
        threadTableView.addSubview(refreshControl)
    }
    @objc private func refreshCurrentThread(){
        refreshControl.attributedTitle = NSAttributedString(string: "")
        let oldReplyCount = arrayPost.count
        let op = arrayPost.first?.info.id
        newPostsGroup = DispatchGroup()
        newPostsGroup?.enter()
        requestAPI.request(url: strWeb, object: .thread, limit: 150, op: op)
        newPostsGroup?.notify(queue: .main, execute: {
            [weak self] in
            self?.arrayPost = arrayKomicaPost
            if oldReplyCount < (self?.arrayPost.count)!{
                let diff = (self?.arrayPost.count)! - oldReplyCount
                self?.alertPopUp(content: "\(diff)則新回覆")
            }
            self?.threadTableView.AnimationReloadData()
            self?.refreshControl.endRefreshing()
            newPostsGroup = nil
        })
    }
    var floatBtn:UIImageView!
    private func floatingAccessoryButton(){
        initialFloatingButton()
        view.addSubview(floatBtn)
    }

    /// 用內建瀏覽器safari打開url
    private func openInSafari(linkOfWebsite link: String?){
        if let url = URL(string: link ?? ""){
            let vcSafari = SFSafariViewController(url: url)
            present(vcSafari, animated: true, completion: nil)
        }
    }
    
    
    /// 用瀏覽器打開網址
    private func showWebsiteWithBrowser(url: String){
        let alertView = UIAlertController.init(title: "選擇網址打開方式", message: nil, preferredStyle: .actionSheet)
        let browerVC = UIAlertAction.init(title: "內建瀏覽器", style: .default, handler: {
            [weak self] action in
            self?.performSegue(withIdentifier: "showBrowser", sender: self)
        })
        browerVC.setValue(view.tintColor, forKey: "titleTextColor")
        
        let safariBrower = UIAlertAction.init(title: "Safari", style: .default, handler: {
            [weak self] action in
            self?.openInSafari(linkOfWebsite: url)
        })
        safariBrower.setValue(view.tintColor, forKey: "titleTextColor")
        
        let actionCancel = UIAlertAction.init(title: "取消", style: .cancel, handler: nil)
        actionCancel.setValue(view.tintColor, forKey: "titleTextColor")
        
        alertView.addAction(browerVC)
        alertView.addAction(safariBrower)
        alertView.addAction(actionCancel)
        present(alertView,animated: true, completion: nil)
    }
    
    
    
    
    
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showSourceImage"{
            guard let image = sender as? komicaImageNode,
            let desVc = segue.destination as? ImgSourceViewController else{
                return
            }
            desVc.imageNode = image
            desVc.titleImage =  desVc.imageNode.debugDescription + desVc.imageNode.ext
        }
        
        let backItem = UIBarButtonItem()
        backItem.title = navigationItem.title
        navigationItem.backBarButtonItem = backItem
//        if segue.identifier == "showBrowser"{
//            let desVc = segue.destination as! BrowserViewController
//            desVc.targetURL = specificThread.linkOrigin ?? ""
//        }
    }
    
    
    
    
    
    
    
    
   
}



extension ThreadViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard (arrayPost.count > 0 ) else {
            return 0
        }
        return arrayPost.count
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

                return UITableView.automaticDimension
            
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.tableFooterView = UIView(frame: .zero)
        var cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ThreadCell
        cell = setCell(cell: cell, indexPath: indexPath)
        return cell
    }
    
    func setCell(cell: ThreadCell, indexPath: IndexPath) -> ThreadCell{
        cell.lblContent.numberOfLines = 0
        cell.lblContent.lineBreakMode = .byWordWrapping
        let post = arrayPost[indexPath.row]
        //  圖片
        if let postImage = post.image{
            cell.imgThumbnailWidth.constant = 80
            cell.imgThumbnail.sd_setImage(with: URL(string: postImage.thumb), placeholderImage: nil)
            if checkGestureAdded(cell, target: "imageSource") == false{
                let tap = UITapGestureRecognizer(target: self, action: #selector(previewImage))
                cell.imgThumbnail.isUserInteractionEnabled = true
                cell.imgThumbnail.addGestureRecognizer(tap)
            }
        }else{
            cell.imgThumbnailWidth.constant = 0
        }
        //  區分原po和回文用
        if cell.gestureRecognizers != nil{
            cell.gestureRecognizers?.removeAll()
        }
        if indexPath.row == 0{
            cell.backgroundColor = .black
            cell.imgThumbnailLeading.constant = 0
            cell.lblContent.font = UIFont.systemFont(ofSize: 18)
        }else{
            cell.backgroundColor = UIColor(displayP3Red: 15/255, green: 15/255,
                                           blue: 15/255, alpha: 1.0)
            cell.imgThumbnailLeading.constant = 15
            cell.lblContent.font = UIFont.systemFont(ofSize: 16)
        }
        ///  取得貼文資訊
        cell.lblDate.text = post.info.date
        cell.lblTime.text = ""
        cell.lblSerialNo.text = "\(post.info.id)"
        cell.lblUniqueID.text = post.info.author.id
        cell.lblContent.text = post.commit
        return cell
    }
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let index = indexPath.row
        guard arrayPost[index].image != nil else{
            return UISwipeActionsConfiguration(actions: [])
        }
        //  新開原圖
        let openImgSource: UIContextualAction = {
            let action = UIContextualAction(style: .normal, title: nil, handler: {
                [weak self] (action,view,handler) in
                guard let strongSelf = self else { return }
                strongSelf.performSegue(withIdentifier: "showSourceImage",
                                        sender: strongSelf.arrayPost[index].image!)
                handler(true)
            })
            return action
        }()
        openImgSource.image = UIImage(named:"icon_Image")
        openImgSource.backgroundColor = tableView.cellForRow(at: indexPath)?.backgroundColor
        return UISwipeActionsConfiguration(actions: [openImgSource])
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        return UISwipeActionsConfiguration(actions: [])
    }
    //  打開預覽圖
    @objc func previewImage(_ sender:UITapGestureRecognizer){
        let location = sender.location(in: threadTableView)
        if let indexPath = threadTableView.indexPathForRow(at: location){
            strImgSourceURL = arrayKomicaPost[indexPath.row].image!.src
            //  將該cell的圖片origin轉化成superview上的座標
            let cell = threadTableView.cellForRow(at: indexPath) as! ThreadCell
            let imgFrameOfCell = cell.imgThumbnail.superview?.convert(cell.imgThumbnail.frame, to: threadTableView.superview)
            
            //  設定預覽圖ImageView
            let imgPreview:UIImageView = {
                //  imgPlaceHolder showed when imgPreview was not Valid
                let cell = threadTableView.cellForRow(at: indexPath) as! ThreadCell
                let imgThumbnail = cell.imgThumbnail.image
                //  the img which to preview
                let imgView = UIImageView()
                imgView.contentMode = .scaleAspectFit
                imgView.frame = imgFrameOfCell!
                imgView.backgroundColor = .clear
                imgView.isUserInteractionEnabled = true
                
                let sourceURL = URL(string: strImgSourceURL)
                switch arrayPost[indexPath.row].image?.ext{
                case ".gif":
                    imgView.sd_setImage(with: nil, placeholderImage: imgThumbnail?.images?.first)
                    DispatchQueue.global().async {
                        do {
                            let gifData = try Data(contentsOf: sourceURL!)
                            DispatchQueue.main.async {
                                let imgGIF = FLAnimatedImage(gifData: gifData)
                                let newImgGIF = FLAnimatedImageView(frame: imgView.bounds)
                                newImgGIF.animatedImage = imgGIF
                                newImgGIF.contentMode = imgView.contentMode
                                imgView.addSubview(newImgGIF)
                            }
                        } catch {
                            print(error)
                        }
                    }
                default:
                    imgView.sd_setImage(with: sourceURL, placeholderImage: imgThumbnail)
                }
                
                
                
                ///  拖曳移除該ImageView
                let dragDismiss = CustomPanGestureRecognizer(target: self, action: #selector(actionDragDismiss))
                dragDismiss.indexPathImage = indexPath
                dragDismiss.rectImage = imgFrameOfCell
                imgView.addGestureRecognizer(dragDismiss)
                /// 長壓儲存圖片
                let longPress = UILongPressGestureRecognizer(target: self, action: #selector(alertSaveImage))
                longPress.minimumPressDuration = 0.4
                imgView.addGestureRecognizer(longPress)
                
                return imgView
            }()
            
            Vibration(.light)
            navigationController?.view.addSubview(layerPreviewBackground)
            navigationController?.view.addSubview(imgPreview)
            cell.imgThumbnail.alpha = 0.0
            UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                [weak self] in
                self?.animationNavigationBar(isHide: true)
                self?.layerPreviewBackground.alpha = 0.8
                if let height = self?.navigationController?.navigationBar.bounds.size.height{
                    let origin = CGPoint(x: 0.0, y: height)
                    let size = CGSize(width: (self?.view.bounds.size.width)!,
                                      height: (self?.view.bounds.size.height)! - height)
                    imgPreview.frame = CGRect(origin: origin, size: size)
                }
                //  防治imgPreview在顯示中可以使用navigationController的swipeBack gesture功能
                self?.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
            })
        }
    }
    /// 隱藏navigationBar
    func animationNavigationBar(isHide: Bool){
        if isHide == false{
            navigationController?.navigationBar.transform = .identity
            return
        }
        if let transfromHeight = navigationController?.navigationBar.bounds.size.height{
            let transform = CGAffineTransform(translationX: 0, y: -transfromHeight*2)
            navigationController?.navigationBar.transform = transform
        }
    }
    //  拖曳下拉取消ViewController
    @objc func actionDragDismiss(_ sender: CustomPanGestureRecognizer){
        let translation = sender.translation(in: view)
        let oldOrigin = sender.view?.bounds.origin
        sender.view?.frame.origin = translation
        if sender.state == .ended {
            let velocity = sender.velocity(in: view)
            let velocityLimit:CGFloat = 1500
            if velocity.y > velocityLimit || velocity.y < -velocityLimit ||
                velocity.x > velocityLimit || velocity.x < -velocityLimit{
                /// dismiss預覽圖的場合
                UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 0.3, options: .curveEaseOut, animations: {
                    [weak self] in
                    sender.view?.frame = sender.rectImage!
                    self?.animationNavigationBar(isHide: false)
                    self?.layerPreviewBackground.alpha = 0.0
                    self?.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
                }, completion: { [weak self](finish:Bool) in
                    let cell = self?.threadTableView.cellForRow(at: sender.indexPathImage!) as! ThreadCell
                    cell.imgThumbnail.alpha = 1.0
                    sender.view?.removeFromSuperview()
                    self?.layerPreviewBackground.removeFromSuperview()
                })
            }else{
                /// 沒dismiss的場合
                UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.1, options: .curveEaseInOut, animations: {
                    sender.view?.frame.origin = oldOrigin!
                })
            }
        }
    }
    /// 警告視窗-儲存圖片
    @objc func alertSaveImage(_ sender: UIGestureRecognizer){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let saveImage = UIAlertAction(title: "Save to Album", style: .default, handler:{ [weak self] action in
            if let imgView = sender.view as? UIImageView{
                self?.saveImageToAlbum(imgView.image)
            }
        })
        alert.addAction(cancel)
        alert.addAction(saveImage)
        present(alert,animated: true,completion: nil)
    }
    
    /// 功能-儲存圖片
    func saveImageToAlbum(_ image: UIImage?){
        guard image != nil else{
            alertPopUp(content: "Image not found!")
            return
        }
        UIImageWriteToSavedPhotosAlbum(image!, self, nil, nil)
        DispatchQueue.main.async {
            [weak self] in
            if var imgSavedCount = UserDefaults.standard.object(forKey: "imgSavedCount") as? Int {
                imgSavedCount += 1
                UserDefaults.standard.set(imgSavedCount, forKey: "imgSavedCount")
            }else{
                UserDefaults.standard.set(1, forKey: "imgSavedCount")
            }
            
            self?.Vibration(.light)
            self?.alertPopUp(content: "Saved")
        }
    }
    
    
    
    
    //  檢查gesture是否曾加入過
    func checkGestureAdded(_ sender:UITableViewCell,target:String) -> Bool{
        if let recognizers = sender.gestureRecognizers {
            for gesture in recognizers {
                switch target{
                case "urlThread":
                    if gesture is UILongPressGestureRecognizer { return true }
                case "imageSource":
                    if gesture is UITapGestureRecognizer {   return true }
                default:        return false
                }
            }
        }
        return false
    }
}
/// floating button related
extension ThreadViewController{
    /// 對floatBtn做初始化動作
    func initialFloatingButton(){
        floatBtn = {
            let length:CGFloat = 50
            let customSize = CGSize(width: length, height: length)
            let customPoint = CGPoint(x: view.bounds.size.width/2 - length/2,
                                      y: view.frame.maxY - 100 - length/2)
            let customFrame = CGRect(origin: customPoint, size: customSize)
            let imgView = UIImageView(frame: customFrame)
            imgView.image = UIImage(named: "icon_App")
            imgView.layer.cornerRadius = length/2
            imgView.alpha = 0.7
            imgView.isUserInteractionEnabled = true
            
            
            //  點擊物件觸發
            let tap:UITapGestureRecognizer = {
                return UITapGestureRecognizer(target: self,
                                              action: #selector(actionReloadCurrentView))
            }()
            //  滑動物件觸發
            let swipeUp:UISwipeGestureRecognizer = {
                return UISwipeGestureRecognizer(target: self,
                                                action: #selector(actionSwipe))
            }()
            swipeUp.direction = .up
            let swipeDown:UISwipeGestureRecognizer = {
                return UISwipeGestureRecognizer(target: self,
                                                action: #selector(actionSwipe))
            }()
            swipeDown.direction = .down
            let swipeRight:UISwipeGestureRecognizer = {
                return UISwipeGestureRecognizer(target: self,
                                                action: #selector(actionSwipe))
            }()
            swipeRight.direction = .right
            //  長壓物件觸發
            let press:UILongPressGestureRecognizer = {
                return UILongPressGestureRecognizer(target: self,
                                                    action: #selector(actionBackSection))
            }()
            press.minimumPressDuration = 0.3
            imgView.addGestureRecognizer(tap)
            imgView.addGestureRecognizer(swipeUp)
            imgView.addGestureRecognizer(swipeDown)
            imgView.addGestureRecognizer(swipeRight)
            imgView.addGestureRecognizer(press)
            return imgView
        }()
    }
    @objc func actionReloadCurrentView(_ sender: UILongPressGestureRecognizer){
        sender.removeTarget(self, action: nil)
        
    }
    @objc func actionBackSection(_ sender: UIGestureRecognizer){
        navigationController?.popViewController(animated: true)
    }
    @objc func actionSwipe(_ sender: UISwipeGestureRecognizer){
        sender.removeTarget(sender.view, action: nil)
        
//        switch sender.state{
//        case .ended:
//            switch sender.direction{
//            case .up:
//                let indexPath = IndexPath(row: specificThread.post.count - 1, section: 0)
//                threadTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
//            case .down:
//                threadTableView.setContentOffset(.zero, animated: true)
//            case .right:
//                navigationController?.popViewController(animated: true)
//            case .left: break
//            default:    break
//            }
//        default:    break
//        }
//        DispatchQueue.main.async {
//            [weak self] in
//            self?.view.addSubview(self!.floatBtn)
//            sender.addTarget(self as Any, action: #selector(self?.actionSwipe))
//        }
    }
}
