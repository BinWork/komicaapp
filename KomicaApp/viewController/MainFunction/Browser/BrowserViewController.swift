//
//  BrowserViewController.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/02/27.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//

import UIKit
import WebKit
class BrowserViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var btnWebControl: UIButton!
    @IBOutlet weak var btnGoURL: UIButton!
    @IBOutlet weak var lblURL: UITextField!
    @IBAction func actionWebControl(_ sender: UIButton) {
        if webView.isLoading{
            webView.stopLoading()
            if progressWebView.isDescendant(of: view){
                removeWebViewProgressBar()
            }
            sender.titleLabel!.text = "Reload"
        }else{
            webView.reload()
            sender.titleLabel!.text = "Stop"
        }
    }
    @IBAction func actionGoURL(_ sender: UIButton) {
        Vibration(.light)
        guard let link = lblURL.text else {
            return
        }
        checkURLAvailable(link)
    }
    var targetURL = String()
    
    private var progressWebView = UIProgressView()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        
        setWebViewGesture()
        checkURLAvailable(targetURL)
        // Do any additional setup after loading the view.
    }
    override func viewDidDisappear(_ animated: Bool) {
        clearWebViewCache()
    }
    
    func setWebViewGesture(){
        let swipeRight = UISwipeGestureRecognizer(target: self,
                                                  action: #selector(actionSwipe))
        swipeRight.direction = .right
        
        let swipeLeft = UISwipeGestureRecognizer(target: self,
                                                 action: #selector(actionSwipe))
        swipeRight.direction = .left
        
        webView.addGestureRecognizer(swipeRight)
        webView.addGestureRecognizer(swipeLeft)
        
    }
    
    /// 檢查連結是否合法
    func checkURLAvailable(_ link: String){
        guard let url = URL(string: link) else{
            alertPopUp(content: "不合法的網址")
            navigationController?.popViewController(animated: true)
            return
        }
        lblURL.text = link
        loadWebURL(url)
    }
    /// 對webView做URL的request
    func loadWebURL(_ url: URL){
        let request = URLRequest(url: url)
        webView.load(request)
        
    }
    @objc func actionSwipe(_ sender: UISwipeGestureRecognizer){
        switch sender.state{
        case .ended:
            switch sender.direction{
            case .right:
                Vibration(.light)
                if webView.canGoBack {
                    webView.goBack()
                }else{
                    alertPopUp(content: "沒有上一頁")
                }
            case .left:
                Vibration(.light)
                if webView.canGoForward{
                    webView.goForward()
                }else{
                    alertPopUp(content: "最新一頁")
                }
            default:    break
            }
        default:    break
        }
    }
    
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        btnWebControl.setTitle("Reload", for: .normal)
    }
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        btnWebControl.setTitle("Stop", for: .normal)
        addWebViewProgressBar()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: ((WKNavigationActionPolicy) -> Void)) {
        switch navigationAction.navigationType {
        case .formSubmitted, .formResubmitted, .backForward, .reload:
            btnWebControl.setTitle("Reload", for: .normal)
            if let url = navigationAction.request.url {
                print(url.absoluteString) // It will give the selected link URL
                lblURL.text = url.absoluteString
            }
        default:
            break
        }
        decisionHandler(.allow)
    }
    func clearWebViewCache(){
        if #available(iOS 9.0, *){
            let websiteDataTypes = NSSet(array: [WKWebsiteDataTypeDiskCache,
                                                 WKWebsiteDataTypeMemoryCache,
                                                 WKWebsiteDataTypeCookies,
                                                 WKWebsiteDataTypeLocalStorage
                ])
            let date = NSDate(timeIntervalSince1970: 0)
            
            WKWebsiteDataStore.default().removeData(ofTypes: websiteDataTypes as! Set<String>, modifiedSince: date as Date, completionHandler:{ })
        }else{
            var libraryPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.libraryDirectory, FileManager.SearchPathDomainMask.userDomainMask, false).first!
            libraryPath += "/Cookies"
            do {
                try FileManager.default.removeItem(atPath: libraryPath)
            } catch {
                print("error")
            }
            URLCache.shared.removeAllCachedResponses()
        }
        
        print("已清除webView的cache")
    }
    
    
    func addWebViewProgressBar(){
        let origin = webView.frame.origin
        let size = CGSize(width: webView.frame.width, height: 3)
        progressWebView.alpha = 1.0
        progressWebView.frame = CGRect(origin: origin, size: size)
        progressWebView.tintColor = view.tintColor
        progressWebView.trackTintColor = .clear
        progressWebView.progress = 0.0
        view.addSubview(progressWebView)
    }
    func removeWebViewProgressBar(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            [weak self] in
            UIView.animate(withDuration: 0.5, animations: {
                [weak self] in
                self?.progressWebView.alpha = 0.0
                }, completion: { [weak self](finish: Bool) in
                    self?.progressWebView.removeFromSuperview()
            })
        })
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if (keyPath == "estimatedProgress") {
            let progressValue = Float(webView.estimatedProgress)
            UIView.animate(withDuration: 0.1, animations: {
              [weak self] in
                self?.progressWebView.progress = progressValue
            })
            if progressValue  == 1.0{
                removeWebViewProgressBar()
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
