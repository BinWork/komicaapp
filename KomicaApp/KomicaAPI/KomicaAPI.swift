//
//  KomicaAPI.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/04/02.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//
import Alamofire

class KomicaAPI{
    /// threads, posts
    internal enum objectKomicaAPI: String{
        case allThreads = "threads"
        case thread = "thread&op={OP}"
        case posts = "posts"
    }
    
    public var currentSectionURL: String?
    public var responseJSONString:String = ""
    

    func request(url: String, object: objectKomicaAPI, limit: Int = 3, op:Int? = nil){
        currentSectionURL = url
        let action: String = {
            var actionSufix = String()
            actionSufix = "\(object.rawValue)&limit=\(limit)"
            if object == .thread{
                guard op != nil else{
                    print("🐼 op value can't be nil in .thread")
                    return ""
                }
                actionSufix = actionSufix.replacingOccurrences(of:"{OP}", with: String(op!))
            }
            return actionSufix
        }()
        let urlRequest = url.replacingOccurrences(of: "index.html", with: "")
        let infix = "pixmicat.php?mode=module&load=mod_ajax&action=\(action)"
        
        var requestURL = urlRequest + infix
        if requestURL.contains("index.htm"){
            requestURL = requestURL.replacingOccurrences(of: "index.htm", with: "")
        }
        print(requestURL)
        
        
        Alamofire.request(requestURL, method: .get, parameters: nil, encoding: JSONEncoding.default).downloadProgress(closure: {
            trackingProgress in
            /// handle progress here
        }).responseJSON(completionHandler: { response in
            guard response.data != nil, response.error == nil else {
                self.handleRequestFailedError(response: response)
                self.handleDispatchGroupLeaving(type: object)
                return
            }
            // to get JSON return value
            if let resultValue = response.result.value {
                if let responseJSON = resultValue as? [String: Any] {
                    guard responseJSON["error"] == nil, responseJSON["msg"] == nil else{
                        print(responseJSON["msg"]!)
                        return
                    }
                    switch object{
                    case .allThreads:
                        self.getAllThreads(JSON: responseJSON)
                    case .thread, .posts:
                        self.getPostDetail(JSON: responseJSON)
                    @unknown default:
                        print("🐼 objectKomicaAPI no found!!")
                    }
                    
                }
            }
        })
    }
    
    private func handleRequestFailedError(response: DataResponse<Any>){
        let statusCode = response.response?.statusCode
        HTTPStatusCheck().statusCodeCheck(statusCode: statusCode)
        let title = "HTTP \(String(describing: statusCode))"
        let message = "link:\n\(String(describing: currentSectionURL))"
        let alert = AlertPop().alert(title: title, message: message,
                                     action: ["確定":.cancel], handler: nil,
                                     preferredStyle: .alert)
        if let topVC = UIApplication.shared.keyWindow?.rootViewController{
            topVC.present(alert, animated: true, completion: nil)
        }
        
        print("🐼 Error found! \(String(describing:response.error?.localizedDescription))")
        print(response)
    }
    private func handleDispatchGroupLeaving(type object: objectKomicaAPI){
        switch object{
        case .allThreads:
            if newAllThreadsGroup != nil{
                newAllThreadsGroup?.leave()
            }
        case .thread, .posts:
            if newPostsGroup != nil{
                newPostsGroup?.leave()
            }
        }
    }
    
    private func getAllThreads(JSON:[String: Any]){
        if let threads = JSON["threads"] as? NSArray{
            let maximumThreadValue = min(threads.count, 10)
            for index in 0...maximumThreadValue - 1{
                if let detail = threads[index] as? [String: Any]{
                    let limit = detail["replies"] as! Int + 1
                    request(url: self.currentSectionURL!, object: .thread,
                            limit: limit, op: detail["no"] as? Int)
                }
            }
        }else{
            print("🐼 \(#function) fail")
        }
    }
    
    private func getPostDetail(JSON: [String: Any]){
        if let posts = JSON["posts"] as? NSArray{
            arrayKomicaPost.removeAll()
            for post in posts{
                if let postDetail = post as? [String: Any]{
                    let post = parsingPost(postDetail)
                    arrayKomicaPost.append(post)
                }
            }
        }else{
            print("🐼getPostDetail fail")
        }
        if newPostsGroup != nil {
            newPostsGroup?.leave()
        }
    }
}
/// parsingPost
extension KomicaAPI{
    private func parsingPost(_ postJSON: [String: Any]) -> komicaPostNode{
        let imageNode = getImageDetail(postJSON)
        let detailNode = getDetailNode(postJSON)
        var commit = postJSON["com"] as! String
        commit = commit.replacingOccurrences(of: "<br />", with: "\n")  //  換行
        commit = commit.replacingOccurrences(of: "&gt;", with: ">")
        commit = commit.replacingOccurrences(of: "&quot;", with: "\"")
        //let replyTarget = getReplyTarget(commit)
        let post = komicaPostNode(image: imageNode, info: detailNode, commit: commit)
        return post
    }
    private func getImageDetail(_ postJSON: [String: Any]) -> komicaImageNode?{
        guard postJSON["image"] != nil else{
            return nil
        }
        let h = postJSON["h"] as! Int
        let w = postJSON["w"] as! Int
        let thumb = "https:\(postJSON["thumb"] as! String)"
        let src = "https:\(postJSON["image"] as! String)"
        let ext = postJSON["ext"] as! String
        return komicaImageNode(thumb: thumb, src: src, ext: ext, height: h, width: w)
    }
    private func getDetailNode(_ postJSON: [String: Any]) -> komicaPostInfoNode{
        let authorNode = getAuthorNode(postJSON)
        let title = postJSON["sub"] as! String
        let date = postJSON["now"] as! String
        let replyTarget = postJSON["resto"] as! Int
        let id = postJSON["no"] as! Int
        return komicaPostInfoNode(author: authorNode,date: date, title: title,
                                  resTo: replyTarget, id: id,replyTo: nil)
    }
    private func getAuthorNode(_ postJSON: [String: Any]) -> komicaAuthorNode{
        let name = postJSON["name"] as! String
        let id = postJSON["id"] as? String
        let mail = postJSON["mail"] as? String
        return komicaAuthorNode(name: name, id: id, mail: mail)
    }
    /// 土法煉鋼的做法 以後請棄用
    private func getReplyTarget(_ content: String) -> Int?{
        let array = Array(content)
        if array[0] == ">" && array[1] == ">"{
        var tempContent = array.dropFirst(2)
            var replyTarget = ""
            repeat{
                replyTarget.append(tempContent.popFirst() ?? Character(""))
            }while(tempContent.first != "\n")
            return Int(replyTarget)
        }
        return nil
    }
}
