//
//  node.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/04/04.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//

import Foundation
var arrayKomicaPost:[komicaPostNode] = []


struct komicaThreadNode{
    var posts: [komicaPostNode]
}
    struct komicaPostNode{
        var image: komicaImageNode?
        var info: komicaPostInfoNode
        var commit: String
    }
        /// 附圖Node
        struct komicaImageNode{
            var thumb: String
            var src: String
            /// 副檔名
            var ext: String
            var height: Int
            var width: Int
        }
        /// 回文的資訊
        struct komicaPostInfoNode{
            var author: komicaAuthorNode
            var date: String
            var title: String
            var resTo: Int?
            var id: Int
            var replyTo: Int?
        }
            struct komicaAuthorNode{
                var name: String
                var id: String?
                var mail: String?
                
            }
