//
//  AppDelegate.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2018/12/25.
//  Copyright © 2018 nwfmbin2. All rights reserved.
//

import UIKit
import UserNotifications
import SDWebImage
@UIApplicationMain



/** 《   UserDefault 一覽  》
 
 
 
    ### Tracking 相關 ###
    `   變數名稱: 屬性   ,   描述   `
 
 
    appLaunchCount: Int                 ,   本app開啟次數
    imgSavedCount:  Int                 ,   手動下載儲存圖片的次數
    threadVisited:  Int                 ,   瀏覽過的貼文串數量
 
    arrayHistory:   [postSummaryNode]   ,   歷史瀏覽紀錄
 
 
 
 
 
 
*/






class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //  statusBar related
        UIApplication.shared.statusBarStyle = .lightContent
        //  navigationBar related
        UINavigationBar.appearance().barTintColor = .black
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.white]
        
        
        /// 追蹤本App開啟次數
        if let appLaunchCount = UserDefaults.standard.object(forKey: "appLaunchCount") as? Int{
            UserDefaults.standard.set(appLaunchCount + 1, forKey: "appLaunchCount")
        }else{
            UserDefaults.standard.set(1, forKey: "appLaunchCount")
        }
        
        
        
        /// 檢查瀏覽紀錄
        loadHistoryAndBookmark()
        
        
        
        
        
        
        
        
        return true 
    }
    
    /// 取消app激活狀態
    func applicationWillResignActive(_ application: UIApplication) {
        //  UpdateAppBadgeNumber(number: arrayHistory.count)
    }
    /// app已進入背景
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    /// app將進入前景
    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    /// app已激活(已進入前景)
    func applicationDidBecomeActive(_ application: UIApplication) {
    }
    /// app將被終止
    func applicationWillTerminate(_ application: UIApplication) {
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
    }

    
    /// 儲存瀏覽紀錄&書籤在手機裡
    func updateStoringOfHistoryAndBookmark(){
        if let dataHistoryArray: Data = try? PropertyListEncoder().encode(arrayHistory){
            UserDefaults.standard.removeObject(forKey: "arrayHistory")
            UserDefaults.standard.set(dataHistoryArray, forKey: "arrayHistory")
        }
        if let dataBookmarkArray: Data = try? PropertyListEncoder().encode(arrayBookmark){
            UserDefaults.standard.removeObject(forKey: "arrayBookmark")
            UserDefaults.standard.set(dataBookmarkArray, forKey: "arrayBookmark")
        }
    }
    
    
    /// 載入過往瀏覽紀錄&書籤
    func loadHistoryAndBookmark(){
        if let dataHistory = UserDefaults.standard.object(forKey: "arrayHistory") as? Data {
            if let history = try? PropertyListDecoder().decode(Array<postSummaryNode>.self,
                                                               from: dataHistory){
                arrayHistory = history
            }
        }
        if let dataBookmark = UserDefaults.standard.object(forKey: "arrayBookmark") as? Data {
            if let bookmark = try? PropertyListDecoder().decode(Array<postSummaryNode>.self,
                                                               from: dataBookmark){
                arrayBookmark = bookmark
            }
        }
    }
    /**
     在appIcon標註數字, 這裡用來呈現瀏覽紀錄數字
     - parameter number: 欲表示的個數
    */
    func UpdateAppBadgeNumber(number: Int) {
        let application = UIApplication.shared
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.badge,.alert,.sound], completionHandler: {
            (success, error) in
        })
        guard application.applicationIconBadgeNumber != number else { return }
        application.applicationIconBadgeNumber = number
        updateStoringOfHistoryAndBookmark()
    }

}

extension UIApplication{
    
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
