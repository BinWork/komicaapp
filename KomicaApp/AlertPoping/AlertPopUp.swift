//
//  AlertPopUp.swift
//  KomicaApp
//
//  Created by nwfmbin2 on 2019/01/05.
//  Copyright © 2019 nwfmbin2. All rights reserved.
//


import UIKit

struct AlertPop{

    func alert(title: String, message:String,
               action:[String:UIAlertAction.Style] ,
               handler: (()->Void)? = nil,
               preferredStyle: UIAlertController.Style) -> UIAlertController{
        let alert = UIAlertController(title: title, message: message,
                                      preferredStyle: preferredStyle)
        for act in action{
            alert.addAction(UIAlertAction(title: act.key, style: act.value, handler:{ (action) in
                handler?()
            }))
        }
        return alert
    }
}
